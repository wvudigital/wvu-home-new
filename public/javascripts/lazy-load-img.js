document.addEventListener("DOMContentLoaded", function() {
  var lazyImages;
  if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
    lazyImagesImg = [].slice.call(document.querySelectorAll("img.lazy"));
    lazyImagesPic = [].slice.call(document.querySelectorAll("picture.lazy"));
    lazyImages = lazyImagesImg.concat(lazyImagesPic);
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          if (lazyImage.tagName === "IMG") {
            lazyImage.src = lazyImage.dataset.src;
            lazyImage.classList.remove("lazy");
            lazyImageObserver.unobserve(lazyImage);
          } else if (lazyImage.tagName === 'PICTURE') {
            var children = lazyImage.children;
            for (var i = 0; i < children.length; i++) {
              var child = children[i];
              if (child.tagName === 'SOURCE') {
                child.srcset = child.dataset.srcset;
              }
            }
            lazyImage.classList.remove("lazy");
            lazyImageObserver.unobserve(lazyImage);
          }
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  } else {
    // handle IE 11 only for regular images
    lazyImagesInPicture = document.querySelectorAll("img.lazyInPicture");
    for (i = 0; i < lazyImagesPrimed.length; ++i) {
      lazyImages[i].setAttribute('src',lazyImages[i].getAttribute('data-src'));
    }
    lazyImages = document.querySelectorAll("img.lazy");
    for (i = 0; i < lazyImages.length; ++i) {
      lazyImages[i].setAttribute('src',lazyImages[i].getAttribute('data-src'));
    }
  }
});
