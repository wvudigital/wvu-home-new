/**
 * A lightweight FB video embed. Based on lite-youtube-embed
 */
class LiteFBEmbed extends HTMLElement {
    constructor() {
        super();

        if (this.getElementsByClassName('className'))
        this.videoId = (this.getAttribute('videoid') !== null) ? encodeURIComponent(this.getAttribute('videoid')) : '';
        this.videoAccount = (this.getAttribute('videoaccount') !== null) ? encodeURIComponent(this.getAttribute('videoaccount')) : '';
        this.playLabel = this.getAttribute('playlabel') ? encodeURIComponent(this.getAttribute('playlabel')) : 'Play';
        this.posterUrl = this.getAttribute('posterurl') ? this.getAttribute('posterurl') : 'https://images.ctfassets.net/udw4veezvy46/7y58Pnlzu5qfiZqL2cMI7i/d8f4c80a0caf36f128d1710fa6a74c28/flying-wv__1_.jpg';

        this.embedURL = 'https://www.facebook.com/plugins/video.php?autoplay=true&height=314&href=https%3A%2F%2Fwww.facebook.com%2F'+this.videoAccount+'%2Fvideos%2F'+this.videoId+'%2F&show_text=false&width=560&t=0';

        // Warm the connection for the poster image
        //LiteFBEmbed.addPrefetch('preload', this.posterUrl, 'image');

    }

    connectedCallback() {
      this.style.backgroundImage = `url("${this.posterUrl}")`;

      let playBtn = this.querySelector('.lty-playbtn');

      if (!playBtn) {
        playBtn = document.createElement('button');
        playBtn.type = 'button';
        playBtn.classList.add('lty-playbtn');
        playBtn.title = decodeURIComponent(this.playLabel);
        this.append(playBtn);
      }

      // On hover (or tap), warm up the TCP connections we're (likely) about to use.
      this.addEventListener('pointerover', LiteFBEmbed.warmConnections, {once: true});

      // Once the user clicks, add the real iframe and drop our play button
      // TODO: In the future we could be like amp-youtube and silently swap in the iframe during idle time
      //   We'd want to only do this for in-viewport or near-viewport ones: https://github.com/ampproject/amphtml/pull/5003
      this.addEventListener('click', e => this.addIframe());
    }

    /**
     * Add a <link rel={preload | preconnect} ...> to the head
     */
    static addPrefetch(kind, url, as) {
      const linkElem = document.createElement('link');
      linkElem.rel = kind;
      linkElem.href = url;
      if (as) {
          linkElem.as = as;
      }
      linkElem.crossorigin = true;
      document.head.append(linkElem);
    }

    /**
     * Begin pre-connecting to warm up the iframe load
     * Since the embed's network requests load within its iframe,
     *   preload/prefetch'ing them outside the iframe will only cause double-downloads.
     * So, the best we can do is warm up a few connections to origins that are in the critical path.
     *
     * Maybe `<link rel=preload as=document>` would work, but it's unsupported: http://crbug.com/593267
     * But TBH, I don't think it'll happen soon with Site Isolation and split caches adding serious complexity.
     */
    static warmConnections() {
        if (LiteFBEmbed.preconnected) return;

        // may need to preconncet to resources
        // LiteFBEmbed.addPrefetch('preconnect', 'https://www.youtube-nocookie.com');
        LiteFBEmbed.preconnected = true;
    }

    addIframe(){
        let titleText = (this.querySelector('.lty-playbtn') !== null) ? this.querySelector('.lty-playbtn').textContent.trim() : 'YouTube Video';
        const iframeHTML = `
<iframe width="558" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
  allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"
  src="${this.embedURL}"
></iframe>
`;
        this.style.backgroundImage = 'none';
        this.insertAdjacentHTML('beforeend', iframeHTML);
        this.classList.add('lyt-activated');
    }
}

// Register custom element
customElements.define('lite-facebook', LiteFBEmbed);
