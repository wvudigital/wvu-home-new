/*! responsive-nav.js 1.0.39
 * https://github.com/viljamis/responsive-nav.js
 * http://responsive-nav.com
 *
 * Copyright (c) 2015 @viljamis
 * Available under the MIT license
 */
!function(e, t, n) {
    "use strict";
    var o = function(o, i) {
        !!t.getComputedStyle || (t.getComputedStyle = function(e) {
            return this.el = e, this.getPropertyValue = function(t) {
                var n = /(\-([a-z]){1})/g;
                return "float" === t && (t = "styleFloat"), n.test(t) && (t = t.replace(n, (function() {
                    return arguments[2].toUpperCase()
                }))), e.currentStyle[t] ? e.currentStyle[t] : null
            }, this
        });
        var s,
            a,
            r,
            l,
            c,
            u,
            d,
            h = function(e, t, n, o) {
                if ("addEventListener" in e)
                    try {
                        e.addEventListener(t, n, o)
                    } catch (i) {
                        if ("object" != typeof n || !n.handleEvent)
                            throw i;
                        e.addEventListener(t, (function(e) {
                            n.handleEvent.call(n, e)
                        }), o)
                    }
                else
                    "attachEvent" in e && ("object" == typeof n && n.handleEvent ? e.attachEvent("on" + t, (function() {
                        n.handleEvent.call(n)
                    })) : e.attachEvent("on" + t, n))
            },
            p = function(e, t, n, o) {
                if ("removeEventListener" in e)
                    try {
                        e.removeEventListener(t, n, o)
                    } catch (i) {
                        if ("object" != typeof n || !n.handleEvent)
                            throw i;
                        e.removeEventListener(t, (function(e) {
                            n.handleEvent.call(n, e)
                        }), o)
                    }
                else
                    "detachEvent" in e && ("object" == typeof n && n.handleEvent ? e.detachEvent("on" + t, (function() {
                        n.handleEvent.call(n)
                    })) : e.detachEvent("on" + t, n))
            },
            f = function(e) {
                if (e.children.length < 1)
                    throw new Error("The Nav container has no containing elements");
                for (var t = [], n = 0; n < e.children.length; n++)
                    1 === e.children[n].nodeType && t.push(e.children[n]);
                return t
            },
            v = function(e, t) {
                for (var n in t)
                    e.setAttribute(n, t[n])
            },
            g = function(e, t) {
                0 !== e.className.indexOf(t) && (e.className += " " + t, e.className = e.className.replace(/(^\s*)|(\s*$)/g, ""))
            },
            m = function(e, t) {
                var n = new RegExp("(\\s|^)" + t + "(\\s|$)");
                e.className = e.className.replace(n, " ").replace(/(^\s*)|(\s*$)/g, "")
            },
            y = function(e, t, n) {
                for (var o = 0; o < e.length; o++)
                    t.call(n, o, e[o])
            },
            b = function(e, t) {
                return e.className && new RegExp("(\\s|^)" + t + "(\\s|$)").test(e.className)
            },
            w = function() {
                for (var e = this, t = a.menuItems; -1 === e.className.indexOf(t);)
                    "li" === e.tagName.toLowerCase() && (-1 !== e.className.indexOf("focus") ? e.className = e.className.replace(" focus", "") : e.className += " focus"),
                    e = e.parentElement
            },
            E = e.createElement("style"),
            x = e.documentElement,
            N = function(t, n) {
                var o;
                for (o in this.options = {
                    animate: !0,
                    transition: 284,
                    label: "Menu",
                    insert: "before",
                    customToggle: "",
                    closeOnNavClick: !1,
                    openPos: "relative",
                    navClass: "nav-collapse",
                    navActiveClass: "js-nav-active",
                    jsClass: "js",
                    enableFocus: !1,
                    enableDropdown: !1,
                    menuItems: "menu-items",
                    subMenu: "sub-menu",
                    openDropdown: "Open sub menu",
                    closeDropdown: "Close sub menu",
                    init: function() {},
                    open: function() {},
                    close: function() {},
                    resizeMobile: function() {},
                    resizeDesktop: function() {}
                }, n)
                    this.options[o] = n[o];
                if (g(x, this.options.jsClass), this.wrapperEl = t.replace("#", ""), e.getElementById(this.wrapperEl))
                    this.wrapper = e.getElementById(this.wrapperEl);
                else {
                    if (!e.querySelector(this.wrapperEl))
                        throw new Error("The nav element you are trying to select doesn't exist");
                    this.wrapper = e.querySelector(this.wrapperEl)
                }
                this.wrapper.inner = f(this.wrapper),
                a = this.options,
                s = this.wrapper,
                this._init(this)
            };
        return N.prototype = {
            destroy: function() {
                if (this._removeStyles(), m(s, "closed"), m(s, "opened"), m(s, a.navClass), m(s, a.navClass + "-" + this.index), m(x, a.navActiveClass), s.removeAttribute("style"), s.removeAttribute("aria-hidden"), p(t, "resize", this, !1), p(t, "focus", this, !1), p(e.body, "touchmove", this, !1), p(r, "touchstart", this, !1), p(r, "touchend", this, !1), p(r, "mouseup", this, !1), p(r, "keyup", this, !1), p(r, "click", this, !1), a.customToggle ? r.removeAttribute("aria-hidden") : r.parentNode.removeChild(r), a.enableDropdown) {
                    var n = this;
                    y(d, (function(e, t) {
                        p(t, "touchstart", n, !1),
                        p(t, "touchend", n, !1),
                        p(t, "mouseup", n, !1),
                        p(t, "keyup", n, !1),
                        p(t, "click", n, !1)
                    }))
                }
            },
            toggle: function() {
                !0 === l && (u ? this.close() : this.open())
            },
            open: function() {
                u || (m(s, "closed"), g(s, "opened"), g(x, a.navActiveClass), g(r, "active"), s.style.position = a.openPos, v(s, {
                    "aria-hidden": "false"
                }), v(s, {
                    "aria-expanded": "true"
                }), v(r, {
                    "aria-expanded": "true"
                }), u = !0, a.open())
            },
            close: function() {
                u && (g(s, "closed"), m(s, "opened"), m(x, a.navActiveClass), m(r, "active"), v(s, {
                    "aria-hidden": "true"
                }), v(s, {
                    "aria-expanded": "false"
                }), v(r, {
                    "aria-expanded": "false"
                }), a.animate ? (l = !1, setTimeout((function() {
                    s.style.position = "absolute",
                    l = !0,
                    a.enableDropdown && (m(s, "dropdown-active"), y(d, (function(e, t) {
                        m(t, "toggled"),
                        m(t.nextSibling, "toggled")
                    })))
                }), a.transition + 10)) : (s.style.position = "absolute", a.enableDropdown && (m(s, "dropdown-active"), y(d, (function(e, t) {
                    m(t, "toggled"),
                    m(t.nextSibling, "toggled")
                })))), u = !1, a.close())
            },
            resize: function() {
                "none" !== t.getComputedStyle(r, null).getPropertyValue("display") ? (c = !0, v(r, {
                    "aria-hidden": "false"
                }), v(s, {
                    "aria-expanded": "false"
                }), v(r, {
                    "aria-expanded": "false"
                }), s.className.match(/(^|\s)closed(\s|$)/) && (v(s, {
                    "aria-hidden": "true"
                }), s.style.position = "absolute"), s.className.match(/(^|\s)closed(\s|$)/) || (v(s, {
                    "aria-expanded": "true"
                }), v(r, {
                    "aria-expanded": "true"
                })), this._createStyles(), this._calcHeight(), a.resizeMobile()) : (c = !1, v(r, {
                    "aria-hidden": "true"
                }), v(s, {
                    "aria-hidden": "false"
                }), s.removeAttribute("aria-expanded"), r.removeAttribute("aria-expanded"), s.style.position = a.openPos, this._removeStyles(), a.resizeDesktop())
            },
            handleEvent: function(e) {
                var n = e || t.event;
                switch (n.type) {
                case "touchstart":
                    this._onTouchStart(n);
                    break;
                case "touchmove":
                    this._onTouchMove(n);
                    break;
                case "touchend":
                case "mouseup":
                    this._onTouchEnd(n);
                    break;
                case "click":
                    this._preventDefault(n);
                    break;
                case "keyup":
                    this._onKeyUp(n);
                    break;
                case "focus":
                case "resize":
                    this.resize(n)
                }
            },
            _init: function() {
                this.index = n++,
                g(s, a.navClass),
                g(s, a.navClass + "-" + this.index),
                g(s, "closed"),
                l = !0,
                u = !1,
                this._closeOnNavClick(),
                this._createToggle(),
                this._transitions(),
                this.resize(),
                this._createFocus(),
                this._createDropdown();
                var o = this;
                setTimeout((function() {
                    o.resize()
                }), 20),
                h(t, "resize", this, !1),
                h(t, "focus", this, !1),
                h(e.body, "touchmove", this, !1),
                h(r, "touchstart", this, !1),
                h(r, "touchend", this, !1),
                h(r, "mouseup", this, !1),
                h(r, "keyup", this, !1),
                h(r, "click", this, !1),
                a.init()
            },
            _createStyles: function() {
                E.parentNode || (E.type = "text/css", e.getElementsByTagName("head")[0].appendChild(E))
            },
            _removeStyles: function() {
                E.parentNode && E.parentNode.removeChild(E)
            },
            _createToggle: function() {
                if (a.customToggle) {
                    var t = a.customToggle.replace("#", "");
                    if (e.getElementById(t))
                        r = e.getElementById(t);
                    else {
                        if (!e.querySelector(t))
                            throw new Error("The custom nav toggle you are trying to select doesn't exist");
                        r = e.querySelector(t)
                    }
                } else {
                    var n = e.createElement("a");
                    n.innerHTML = a.label,
                    v(n, {
                        href: "#",
                        class: "nav-toggle"
                    }),
                    "after" === a.insert ? s.parentNode.insertBefore(n, s.nextSibling) : s.parentNode.insertBefore(n, s),
                    r = n
                }
            },
            _closeOnNavClick: function() {
                if (a.closeOnNavClick) {
                    var e = s.getElementsByTagName("a"),
                        t = this;
                    y(e, (function(n) {
                        h(e[n], "click", (function() {
                            c && t.toggle()
                        }), !1)
                    }))
                }
            },
            _preventDefault: function(e) {
                if (e.preventDefault)
                    return e.stopImmediatePropagation && e.stopImmediatePropagation(), e.preventDefault(), e.stopPropagation(), !1;
                e.returnValue = !1
            },
            _onTouchStart: function(e) {
                Event.prototype.stopImmediatePropagation || this._preventDefault(e),
                this.startX = e.touches[0].clientX,
                this.startY = e.touches[0].clientY,
                this.touchHasMoved = !1,
                p(r, "mouseup", this, !1)
            },
            _onTouchMove: function(e) {
                (Math.abs(e.touches[0].clientX - this.startX) > 10 || Math.abs(e.touches[0].clientY - this.startY) > 10) && (this.touchHasMoved = !0)
            },
            _onTouchEnd: function(e) {
                if (this._preventDefault(e), c) {
                    var n = e || t.event,
                        o = n.target || n.srcElement,
                        i = !1;
                    if (b(o, "dropdown-toggle") && a.enableDropdown && (i = !0), !this.touchHasMoved) {
                        if ("touchend" === e.type)
                            return void (i ? this._toggleDropdown(o) : this.toggle());
                        var s = e || t.event;
                        3 !== s.which && 2 !== s.button && (i ? this._toggleDropdown(o) : this.toggle())
                    }
                }
            },
            _onKeyUp: function(e) {
                var n = e || t.event,
                    o = e.target,
                    i = !1;
                b(o, "dropdown-toggle") && a.enableDropdown && (i = !0),
                13 === n.keyCode && (i ? this._toggleDropdown(o) : this.toggle())
            },
            _transitions: function() {
                if (a.animate) {
                    var e = s.style,
                        t = "max-height " + a.transition + "ms, visibility " + a.transition + "ms linear";
                    e.WebkitTransition = e.MozTransition = e.OTransition = e.transition = t
                }
            },
            _calcHeight: function() {
                for (var e = 0, t = 0; t < s.inner.length; t++)
                    e += s.inner[t].offsetHeight;
                var n = "." + a.jsClass + " ." + a.navClass + "-" + this.index + ".opened{max-height:" + e + "px !important} ." + a.jsClass + " ." + a.navClass + "-" + this.index + ".opened.dropdown-active {max-height:9999px !important}";
                E.styleSheet ? E.styleSheet.cssText = n : E.innerHTML = n,
                n = ""
            },
            _createFocus: function() {
                if (a.enableFocus) {
                    var e,
                        t,
                        n = s.getElementsByTagName("ul")[0].getElementsByTagName("a");
                    for (t = 0, e = n.length; t < e; t++)
                        n[t].addEventListener("focus", w, !0),
                        n[t].addEventListener("blur", w, !0)
                }
            },
            _createDropdown: function() {
                if (a.enableDropdown) {
                    var e,
                        t,
                        n = this,
                        o = (s.getElementsByTagName("ul")[0], s.getElementsByClassName(a.subMenu));
                    for (g(s, "multiple-level-nav"), e = 0, t = o.length; e < t; e++)
                        o[e].parentNode.setAttribute("aria-haspopup", "true"),
                        o[e].insertAdjacentHTML("beforebegin", '<button class="dropdown-toggle" aria-expanded="false">' + a.openDropdown + "</button>");
                    d = s.querySelectorAll(".dropdown-toggle"),
                    y(d, (function(e, t) {
                        h(t, "touchstart", n, !1),
                        h(t, "touchend", n, !1),
                        h(t, "mouseup", n, !1),
                        h(t, "keyup", n, !1),
                        h(t, "click", n, !1)
                    }))
                }
            },
            _toggleDropdown: function(e) {
                e.innerHTML === a.openDropdown ? e.innerHTML = a.closeDropdown : e.innerHTML = a.openDropdown;
                var t = e.parentNode;
                b(t.parentNode.parentNode, "dropdown");
                if (b(e, "toggled")) {
                    m(e, "toggled"),
                    e.setAttribute("aria-expanded", "false");
                    n = e.nextElementSibling;
                    m(n, "toggled"),
                    m(s, "dropdown-active")
                } else {
                    g(e, "toggled"),
                    e.setAttribute("aria-expanded", "true");
                    var n = e.nextElementSibling;
                    g(n, "toggled"),
                    g(s, "dropdown-active")
                }
            }
        }, new N(o, i)
    };
    "undefined" != typeof module && module.exports ? module.exports = o : t.responsiveNav = o
}(document, window, 0);
var customToggle = document.getElementById("nav-toggle"),
    navigation = responsiveNav(".nav-collapse", {
        animate: !0,
        insert: "before",
        transition: 600,
        customToggle: "#nav-toggle",
        enableFocus: !0,
        enableDropdown: !0,
        menuItems: "menu-items",
        subMenu: "sub-menu",
        openPos: "relative",
        openDropdown: '<span class="sr-only">Open sub menu</span>',
        closeDropdown: '<span class="sr-only">Close sub menu</span>',
        open: function() {
            customToggle.innerHTML = '<img class="wvu-nav__menu-icon" src="https://static.wvu.edu/global/images/icons/wvu/hamburger-menu--1.0.0.svg" alt="" />Close Menu'
        },
        close: function() {
            customToggle.innerHTML = '<img class="wvu-nav__menu-icon" src="https://static.wvu.edu/global/images/icons/wvu/hamburger-menu--1.0.0.svg" alt="" />Open Menu'
        },
        resizeMobile: function() {
            customToggle.setAttribute("aria-controls", "wvu-nav")
        },
        resizeDesktop: function() {
            customToggle.removeAttribute("aria-controls")
        }
    });
!function(e, t) {
    "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : t()
}(0, (function() {
    function e(e) {
        return e.replace(/<b[^>]*>(.*?)<\/b>/gi, (function(e, t) {
            return t
        })).replace(/class="(?!(tco-hidden|tco-display|tco-ellipsis))+.*?"|data-query-source=".*?"|dir=".*?"|rel=".*?"/gi, "")
    }
    function t(e) {
        for (var t = e.getElementsByTagName("a"), i = t.length - 1; i >= 0; i--)
            t[i].setAttribute("target", "_blank"),
            t[i].setAttribute("rel", "noopener")
    }
    function i(e, t) {
        for (var i = [], n = new RegExp("(^| )" + t + "( |$)"), a = e.getElementsByTagName("*"), l = 0, s = a.length; l < s; l++)
            n.test(a[l].className) && i.push(a[l]);
        return i
    }
    function n(e) {
        if (void 0 !== e && e.innerHTML.indexOf("data-image") >= 0) {
            for (var t = e.innerHTML.match(/data-image=\"([^"]+)\"/gi), i = 0; i < t.length; i++)
                t[i] = t[i].match(/data-image=\"([^"]+)\"/i)[1],
                t[i] = decodeURIComponent(t[i]) + ".jpg";
            return t
        }
    }
    var a = "",
        l = 20,
        s = !0,
        r = [],
        o = !1,
        c = !0,
        m = !0,
        d = null,
        p = !0,
        u = !0,
        g = null,
        h = !0,
        w = !1,
        f = !1,
        b = !0,
        v = !0,
        _ = !1,
        y = null,
        T = {
            fetch: function(e) {
                if (void 0 === e.maxTweets && (e.maxTweets = 20), void 0 === e.enableLinks && (e.enableLinks = !0), void 0 === e.showUser && (e.showUser = !0), void 0 === e.showTime && (e.showTime = !0), void 0 === e.dateFunction && (e.dateFunction = "default"), void 0 === e.showRetweet && (e.showRetweet = !0), void 0 === e.customCallback && (e.customCallback = null), void 0 === e.showInteraction && (e.showInteraction = !0), void 0 === e.showImages && (e.showImages = !1), void 0 === e.useEmoji && (e.useEmoji = !1), void 0 === e.linksInNewWindow && (e.linksInNewWindow = !0), void 0 === e.showPermalinks && (e.showPermalinks = !0), void 0 === e.dataOnly && (e.dataOnly = !1), o)
                    r.push(e);
                else {
                    o = !0,
                    a = e.domId,
                    l = e.maxTweets,
                    s = e.enableLinks,
                    m = e.showUser,
                    c = e.showTime,
                    u = e.showRetweet,
                    d = e.dateFunction,
                    g = e.customCallback,
                    h = e.showInteraction,
                    w = e.showImages,
                    f = e.useEmoji,
                    b = e.linksInNewWindow,
                    v = e.showPermalinks,
                    _ = e.dataOnly;
                    var t = document.getElementsByTagName("head")[0];
                    null !== y && t.removeChild(y),
                    (y = document.createElement("script")).type = "text/javascript",
                    void 0 !== e.list ? y.src = "https://syndication.twitter.com/timeline/list?callback=__twttrf.callback&dnt=false&list_slug=" + e.list.listSlug + "&screen_name=" + e.list.screenName + "&suppress_response_codes=true&lang=" + (e.lang || "en") + "&rnd=" + Math.random() : void 0 !== e.profile ? y.src = "https://syndication.twitter.com/timeline/profile?callback=__twttrf.callback&dnt=false&screen_name=" + e.profile.screenName + "&suppress_response_codes=true&lang=" + (e.lang || "en") + "&rnd=" + Math.random() : void 0 !== e.likes ? y.src = "https://syndication.twitter.com/timeline/likes?callback=__twttrf.callback&dnt=false&screen_name=" + e.likes.screenName + "&suppress_response_codes=true&lang=" + (e.lang || "en") + "&rnd=" + Math.random() : void 0 !== e.collection ? y.src = "https://syndication.twitter.com/timeline/collection?callback=__twttrf.callback&dnt=false&collection_id=" + e.collection.collectionId + "&suppress_response_codes=true&lang=" + (e.lang || "en") + "&rnd=" + Math.random() : y.src = "https://cdn.syndication.twimg.com/widgets/timelines/" + e.id + "?&lang=" + (e.lang || "en") + "&callback=__twttrf.callback&suppress_response_codes=true&rnd=" + Math.random(),
                    t.appendChild(y)
                }
            },
            callback: function(y) {
                function k(e) {
                    var t = e.getElementsByTagName("img")[0];
                    if (t)
                        t.src = t.getAttribute("data-src-2x");
                    else {
                        var i = e.getElementsByTagName("a")[0].getAttribute("href").split("twitter.com/")[1],
                            n = document.createElement("img");
                        n.setAttribute("src", "https://twitter.com/" + i + "/profile_image?size=bigger"),
                        e.prepend(n)
                    }
                    return e
                }
                if (void 0 === y || void 0 === y.body)
                    return o = !1, void (r.length > 0 && (T.fetch(r[0]), r.splice(0, 1)));
                f || (y.body = y.body.replace(/(<img[^c]*class="Emoji[^>]*>)|(<img[^c]*class="u-block[^>]*>)/g, "")),
                w || (y.body = y.body.replace(/(<img[^c]*class="NaturalImage-image[^>]*>|(<img[^c]*class="CroppedImage-image[^>]*>))/g, "")),
                m || (y.body = y.body.replace(/(<img[^c]*class="Avatar"[^>]*>)/g, ""));
                var C = document.createElement("div");
                C.innerHTML = y.body,
                void 0 === C.getElementsByClassName && (p = !1);
                var E = [],
                    x = [],
                    N = [],
                    A = [],
                    B = [],
                    I = [],
                    M = [],
                    L = 0;
                if (p)
                    for (var j = C.getElementsByClassName("timeline-Tweet"); L < j.length;)
                        j[L].getElementsByClassName("timeline-Tweet-retweetCredit").length > 0 ? B.push(!0) : B.push(!1),
                        (!B[L] || B[L] && u) && (E.push(j[L].getElementsByClassName("timeline-Tweet-text")[0]), I.push(j[L].getAttribute("data-tweet-id")), m && x.push(k(j[L].getElementsByClassName("timeline-Tweet-author")[0])), N.push(j[L].getElementsByClassName("dt-updated")[0]), M.push(j[L].getElementsByClassName("timeline-Tweet-timestamp")[0]), void 0 !== j[L].getElementsByClassName("timeline-Tweet-media")[0] ? A.push(j[L].getElementsByClassName("timeline-Tweet-media")[0]) : A.push(void 0)),
                        L++;
                else
                    for (j = i(C, "timeline-Tweet"); L < j.length;)
                        i(j[L], "timeline-Tweet-retweetCredit").length > 0 ? B.push(!0) : B.push(!1),
                        (!B[L] || B[L] && u) && (E.push(i(j[L], "timeline-Tweet-text")[0]), I.push(j[L].getAttribute("data-tweet-id")), m && x.push(k(i(j[L], "timeline-Tweet-author")[0])), N.push(i(j[L], "dt-updated")[0]), M.push(i(j[L], "timeline-Tweet-timestamp")[0]), void 0 !== i(j[L], "timeline-Tweet-media")[0] ? A.push(i(j[L], "timeline-Tweet-media")[0]) : A.push(void 0)),
                        L++;
                E.length > l && (E.splice(l, E.length - l), x.splice(l, x.length - l), N.splice(l, N.length - l), B.splice(l, B.length - l), A.splice(l, A.length - l), M.splice(l, M.length - l));
                var H = [],
                    P = (L = E.length, 0);
                if (_)
                    for (; P < L;)
                        H.push({
                            tweet: E[P].innerHTML,
                            tweetText: E[P].textContent,
                            author: x[P] ? x[P].innerHTML : "Unknown Author",
                            author_data: {
                                profile_url: x[P] ? x[P].querySelector('[data-scribe="element:user_link"]').href : null,
                                profile_image: x[P] ? "https://twitter.com/" + x[P].querySelector('[data-scribe="element:screen_name"]').title.split("@")[1] + "/profile_image?size=bigger" : null,
                                profile_image_2x: x[P] ? "https://twitter.com/" + x[P].querySelector('[data-scribe="element:screen_name"]').title.split("@")[1] + "/profile_image?size=original" : null,
                                screen_name: x[P] ? x[P].querySelector('[data-scribe="element:screen_name"]').title : null,
                                name: x[P] ? x[P].querySelector('[data-scribe="element:name"]').title : null
                            },
                            time: N[P].textContent,
                            timestamp: N[P].getAttribute("datetime").replace("+0000", "Z").replace(/([\+\-])(\d\d)(\d\d)/, "$1$2:$3"),
                            image: n(A[P]) ? n(A[P])[0] : void 0,
                            images: n(A[P]),
                            rt: B[P],
                            tid: I[P],
                            permalinkURL: void 0 === M[P] ? "" : M[P].href
                        }),
                        P++;
                else
                    for (; P < L;) {
                        if ("string" != typeof d) {
                            var R = N[P].getAttribute("datetime"),
                                F = new Date(N[P].getAttribute("datetime").replace(/-/g, "/").replace("T", " ").split("+")[0]),
                                S = d(F, R);
                            if (N[P].setAttribute("aria-label", S), E[P].textContent)
                                if (p)
                                    N[P].textContent = S;
                                else {
                                    var q = document.createElement("p"),
                                        O = document.createTextNode(S);
                                    q.appendChild(O),
                                    q.setAttribute("aria-label", S),
                                    N[P] = q
                                }
                            else
                                N[P].textContent = S
                        }
                        var U = "";
                        if (s ? (b && (t(E[P]), m && t(x[P])), m && (U += '<div class="user">' + e(x[P].innerHTML) + "</div>"), U += '<p class="tweet">' + e(E[P].innerHTML) + "</p>", c && (U += v ? '<p class="timePosted"><a href="' + M[P] + '">' + N[P].getAttribute("aria-label") + "</a></p>" : '<p class="timePosted">' + N[P].getAttribute("aria-label") + "</p>")) : (E[P].textContent, m && (U += '<p class="user">' + x[P].textContent + "</p>"), U += '<p class="tweet">' + E[P].textContent + "</p>", c && (U += '<p class="timePosted">' + N[P].textContent + "</p>")), h && (U += '<p class="interact"><a href="https://twitter.com/intent/tweet?in_reply_to=' + I[P] + '" class="twitter_reply_icon"' + (b ? ' target="_blank" rel="noopener">' : ">") + 'Reply</a><a href="https://twitter.com/intent/retweet?tweet_id=' + I[P] + '" class="twitter_retweet_icon"' + (b ? ' target="_blank" rel="noopener">' : ">") + 'Retweet</a><a href="https://twitter.com/intent/favorite?tweet_id=' + I[P] + '" class="twitter_fav_icon"' + (b ? ' target="_blank" rel="noopener">' : ">") + "Favorite</a></p>"), w && void 0 !== A[P] && void 0 !== n(A[P]))
                            for (var D = n(A[P]), $ = 0; $ < D.length; $++)
                                U += '<div class="media"><img src="' + D[$] + '" alt="Image from tweet" /></div>';
                        (w || !w && E[P].textContent.length) && H.push(U),
                        P++
                    }
                !function(e) {
                    if (null === g) {
                        for (var t = e.length, i = 0, n = document.getElementById(a), l = "<ul>"; i < t;)
                            l += "<li>" + e[i] + "</li>",
                            i++;
                        l += "</ul>",
                        n.innerHTML = l
                    } else
                        g(e)
                }(H),
                o = !1,
                r.length > 0 && (T.fetch(r[0]), r.splice(0, 1))
            }
        };
    return window.__twttrf = T, window.twitterFetcher = T, T
})),
[Element.prototype, Document.prototype, DocumentFragment.prototype].forEach((function(e) {
    e.hasOwnProperty("prepend") || Object.defineProperty(e, "prepend", {
        configurable: !0,
        enumerable: !0,
        writable: !0,
        value: function() {
            var e = Array.prototype.slice.call(arguments),
                t = document.createDocumentFragment();
            e.forEach((function(e) {
                var i = e instanceof Node;
                t.appendChild(i ? e : document.createTextNode(String(e)))
            })),
            this.insertBefore(t, this.firstChild)
        }
    })
}));
