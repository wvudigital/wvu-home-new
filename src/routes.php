<?php

use Slim\Http\Request;
use Slim\Http\Response;

function newrelic_route($route){
  if (extension_loaded('newrelic')) {
    newrelic_name_transaction($route);
  }
}

// Routes
$app->get('/', function (Request $request, Response $response, array $args) {

    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    $this->logger->info("Homepage route ${slug}");
    $page = new \Wvu\Controllers\HomeController;

    // let us use the catch instead
    //$data = $page->view($request);

    try {
      $data = $page->view($request);
    } catch (\Wvu\Services\ServiceError $e) {
      $this->logger->info("Error route: ${slug}");
      $page = new \Wvu\Controllers\ErrorController;
      $data = $page->view($request);
      $data['status'] = (int)$e->code;

      $data['contentful'] = array();
      if ($data['status'] === 404) {
        $data['contentful']['title'] = "Page Not Found";
      } else if ($data['status'] === 500) {
        $data['contentful']['title'] = "Forbidden";
      }
      $data['contentful']['slug'] = '/';

      // Render error view
      return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
    }

    // Name for NewRelic
    newrelic_route("/home");

    // Render index view
    return $this->view->render($response, 'index.html', $data);
});

$app->get('/emergency-test', function( Request $request, Response $response, array $args){
  $this->logger->info("Emergency Test route");

  $page = new \Wvu\Controllers\SiteMapController;
  $data = $page->view($request);

  // Name for NewRelic
  newrelic_route('/emergency-test');

  // Render emergency-test view
  $this->view->render($response, 'emergency-test.html', $data);
});

$app->get('/sitemap', function (Request $request, Response $response, array $args) {
  // Log message
  $this->logger->info("Site Map route");

  $page = new \Wvu\Controllers\SiteMapController;
  $data = $page->view($request);

  // Name for NewRelic
  newrelic_route("/sitemap");

  $this->view->render($response, 'sitemap.html', $data);
})->setName('sitemap');

$app->get('/sitemap.xml', function (Request $request, Response $response, array $args) {
  // Log message
  $this->logger->info("Site Map XML route");

  $page = new \Wvu\Controllers\SiteMapController;
  $data = $page->view($request);

  // Name for NewRelic
  newrelic_route("/sitemap.xml");

  $this->view->render($response, 'sitemap.xml', $data);
  return $this->response->withHeader('Content-Type','text/xml');
});

$app->get('/siteindex', function (Request $request, Response $response, array $args) {

    // Log message
    $this->logger->info("Site Index route");

    $page = new \Wvu\Controllers\SiteIndexController;
    $data = $page->view($request);

    // Name for NewRelic
    newrelic_route("/siteindex");

    // Render siteindex view
    return $this->view->render($response, 'siteindex.html', $data);
})->setName('siteindex');

// locking in the 404 route for the calendar app
$app->get('/404', function (Request $request, Response $response, array $args) {

  $page = new \Wvu\Controllers\ErrorController;
  $data = $page->view($request);
  $data['status'] = 404;
  $data['contentful'] = [];
  $data['contentful']['title'] = 'Page Not Found';
  $data['contentful']['slug'] = '404';

  // Render error view
  return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);

});

$app->get('/error/{page:.*500|.*503|.*404|.*403}', function (Request $request, Response $response, array $args) {

    // Get Route for log
    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    // Log message
    $this->logger->info("Error route: ${slug}");

    $page = new \Wvu\Controllers\ErrorController;
    $data = $page->view($request);
    $data['status'] = (int)$data['page']['route'];

    $data['contentful'] = array();
    if ($data['status'] === 404) {
      $data['contentful']['title'] = "Page Not Found";
    } else if ($data['status'] === 500) {
      $data['contentful']['title'] = "Forbidden";
    }
    $data['contentful']['slug'] = '/error/'.$slug;

    // Name for NewRelic
    newrelic_route("/error/*");

    // Render error view
    return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
});

$app->get('/academics/major-maps/{page:.*}', function (Request $request, Response $response, array $args) {

    // Get Route for log
    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    // Log message
    $this->logger->info("Page route: ${slug}");
    $majorMap = new \Wvu\Controllers\MajorMapController;

    try {
      $data = $majorMap->view($request);
    } catch (\Wvu\Services\ServiceError $e) {
      $page = new \Wvu\Controllers\ErrorController;
      $data = $page->view($request);
      $data['status'] = (int)$e->code;

      $data['contentful'] = array();
      if ($data['status'] === 404) {
        $data['contentful']['title'] = "Page Not Found";
      } else if ($data['status'] === 500) {
        $data['contentful']['title'] = "Forbidden";
      }
      $data['contentful']['slug'] = 'academics/major-maps/'.$slug;

      $this->logger->info("Error ".$e->code." on route: ${slug}");
      newrelic_route("/error/*");

      // Render error view
      return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
    }
    newrelic_route($slug);

    // Render template defined by data
    return $this->view->render($response, 'major-map.html', $data);
});

$app->get('/academics/careers/{page:.*}', function (Request $request, Response $response, array $args) {

    // Get Route for log
    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    // Log message
    $this->logger->info("Page route: ${slug}");
    $career = new \Wvu\Controllers\CareerController;

    try {
      $data = $career->view($request);
      $data['contentful']['slug'] = '/academics/careers/'.$slug;
      $data['contentful']['title'] = 'Example Career: '.$data['career']['title'];
    } catch (\Wvu\Services\ServiceError $e) {
      $page = new \Wvu\Controllers\ErrorController;
      $data = $page->view($request);
      $data['status'] = (int)$e->code;

      $data['contentful'] = array();
      if ($data['status'] === 404) {
        $data['contentful']['title'] = "Page Not Found";
      } else if ($data['status'] === 500) {
        $data['contentful']['title'] = "Forbidden";
      }
      $data['contentful']['slug'] = 'academics/careers/'.$slug;

      $this->logger->info("Error ".$e->code." on route: ${slug}");
      newrelic_route("/error/*");

      // Render error view
      return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
    }
    newrelic_route($slug);

    // Render template defined by data
    return $this->view->render($response, 'career-information.html', $data);
});

$app->get('/academics/programs/{page:[A-z0-9-]+}', function (Request $request, Response $response, array $args) {

    // Get Route for log
    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    // Log message
    $this->logger->info("Page route: ${slug}");
    $program = new \Wvu\Controllers\MajorController;

    try {

      $data = $program->view($request);
      $data['contentful']['slug'] = 'academics/programs/'.$slug;
      $data['contentful']['title'] = $data['major']['title'];
      if (isset($data['major']['degreeDesignation'])) {
        $data['contentful']['title'] = $data['contentful']['title'];
        if ($data['major']['hepcLevelKey'] !== 'bachelors') {
          $data['contentful']['title'] .= ' '.$data['major']['degreeDesignation']['abbreviation'];
        }
        $data['contentful']['title'] .= ' Major';
      }

      // add content to populate the page layout for design system v2
      $data['contentful']['lastUpdated'] = $data['major']['lastUpdated'];
      $data['contentful']['site'] = array();
      $data['contentful']['site']['extraOptions'] = array('Hide Masthead Title');
      $data['contentful']['site']['mastheadQuicklinks'] = array();
      if ($data['major']['hepcLevelKey'] == "bachelor") {
        $data['contentful']['site']['mastheadQuicklinks'][] = array('webAddress' => 'https://admissions.wvu.edu/request-information', 'linkText' => 'Request Info');
        $data['contentful']['site']['mastheadQuicklinks'][] = array('webAddress' => 'https://www.wvu.edu/admissions/experience-wvu', 'linkText' => 'Visit');
        $data['contentful']['site']['mastheadQuicklinks'][] = array('webAddress' => 'https://admissions.wvu.edu/how-to-apply', 'linkText' => 'Apply');
      } else {
        $data['contentful']['site']['mastheadQuicklinks'][] = array('webAddress' => 'https://graduateadmissions.wvu.edu/request-information', 'linkText' => 'Request Info');
        $data['contentful']['site']['mastheadQuicklinks'][] = array('webAddress' => 'https://www.wvu.edu/admissions/experience-wvu', 'linkText' => 'Visit');
        $data['contentful']['site']['mastheadQuicklinks'][] = array('webAddress' => 'https://graduateadmissions.wvu.edu/how-to-apply', 'linkText' => 'Apply');
      }

    } catch (\Wvu\Services\ServiceError $e) {
      $page = new \Wvu\Controllers\ErrorController;
      $data = $page->view($request);
      $data['status'] = (int)$e->code;

      $data['contentful'] = array();
      if ($data['status'] === 404) {
        $data['contentful']['title'] = "Page Not Found";
      } else if ($data['status'] === 500) {
        $data['contentful']['title'] = "Forbidden";
      }
      $data['contentful']['slug'] = 'academics/programs/'.$slug;
      $data['contentful']['site'] = array("extraOptions" => array("Hide Masthead Title"));

      $this->logger->info("Error ".$e->code." on route: ${slug}");
      newrelic_route("/error/*");

      // Render error view
      return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
    }
    newrelic_route($slug);

    // If there is a value in the "Web Request - Redirect" field of this major/program, we want to
    // redirect to the given URL. 
    if (isset($data['major']['webAddressRedirect']) && trim($data['major']['webAddressRedirect']) != '') {
      $this->logger->info("Redirecting program page to: ".$data['major']['webAddressRedirect']);  
      return $response->withStatus(301)->withHeader('Location', $data['major']['webAddressRedirect']);
    }

    // Render major/program template with $data
    $this->view->getLoader()->prependPath(__DIR__.'/../templates/design-system-v2/');
    return $this->view->render($response, 'major-information.html', $data);
});

/*
  $app->get('/academics/courses/{page:.*}', function (Request $request, Response $response, array $args) {

      // Get Route for log
      $route = $request->getAttribute('route');
      $slug = $route->getArgument('page');

      // Log message
      $this->logger->info("Page route: ${slug}");
      $career = new \Wvu\Controllers\CourseController;

      try {
        $data = $career->view($request);
      } catch (\Wvu\Services\ServiceError $e) {
        $page = new \Wvu\Controllers\ErrorController;
        $data = $page->view($request);
        $data['status'] = (int)$e->code;

        $data['contentful'] = array();
        if ($data['status'] === 404) {
          $data['contentful']['title'] = "Page Not Found";
        } else if ($data['status'] === 500) {
          $data['contentful']['title'] = "Forbidden";
        }
        $data['contentful']['slug'] = 'academics/courses/'.$slug;

        $this->logger->info("Error ".$e->code." on route: ${slug}");
        newrelic_route("/error/*");

        // Render error view
        return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
      }
      newrelic_route($slug);

      // Render template defined by data
      return $this->view->render($response, 'course-information.html', $data);
  });
*/

$app->get('/faq/{page:.*}', function (Request $request, Response $response, array $args) {

    // Get Route for log
    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    // Log message
    $this->logger->info("Page route: ${slug}");
    $faq = new \Wvu\Controllers\FAQController;

    try {
      $data = $faq->view($request);
      $data['contentful'] = array();
      $data['contentful']['slug'] = 'faq/'.$slug;
      $data['contentful']['title'] = 'FAQ: '.$data['faq']['title'];
    } catch (\Wvu\Services\ServiceError $e) {
      $page = new \Wvu\Controllers\ErrorController;
      $data = $page->view($request);
      $data['status'] = (int)$e->code;

      $data['contentful'] = array();
      if ($data['status'] === 404) {
        $data['contentful']['title'] = "Page Not Found";
      } else if ($data['status'] === 500) {
        $data['contentful']['title'] = "Forbidden";
      }
      $data['contentful']['slug'] = 'faq/'.$slug;

      $this->logger->info("Error ".$e->code." on route: ${slug}");
      newrelic_route("/error/*");

      // Render error view
      return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);
    }
    newrelic_route($slug);

    // Render template defined by data
    return $this->view->render($response, 'faq-information.html', $data);
});

$app->get('/debug-9080947745', function (Request $request, Response $response, array $args) {
  foreach ($_SERVER as $key => $val) {
    if (strpos($key,'HTTP_CLOUDFRONT_VIEWER') !== false) {
      print $key.": ".str_replace('HTTP_CLOUDFRONT_VIEWER_','',$val)."<br>";
    }
  }
});

$app->get('/{page:.*}', function (Request $request, Response $response, array $args) {

    // Get Route for log
    $route = $request->getAttribute('route');
    $slug = $route->getArgument('page');

    // Log message
    $this->logger->info("Page route: ${slug}");
    $page = new \Wvu\Controllers\PageController;

    try {
      $data = $page->view($request);
    } catch (\Wvu\Services\ServiceError $e) {

      $page = new \Wvu\Controllers\ErrorController;
      $data = $page->view($request);
      $data['status'] = (int)$e->code;
      $data['contentful'] = array();
      if ($data['status'] === 404) {
        $data['contentful']['title'] = "Page Not Found";
      } else if ($data['status'] === 500) {
        $data['contentful']['title'] = "Forbidden";
      }
      $data['contentful']['slug'] = $slug;

      $this->logger->info("Error ".$e->code." on route: ${slug}");
      newrelic_route("/error/*");

      // Render error view
      return $this->view->render($response, 'error.html', $data)->withStatus($data['status']);

    }
    newrelic_route($slug);

    // Render template defined by data
    $themeFolder = '';
    if (isset($data['contentful']['site']) && isset($data['contentful']['site']['theme']) && isset($data['contentful']['site']['theme']['themeFolder'])) {
      $themeFolder = $data['contentful']['site']['theme']['themeFolder'];
    }
    if (!empty($themeFolder)) {
      $themePath = __DIR__.'/../templates/'.$themeFolder.'/';
      $this->view->getLoader()->prependPath($themePath);
    }
    return $this->view->render($response, $data['contentful']['template'], $data);

});
