<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

use Psr7Middlewares\Middleware;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\Stream;

Middleware::setStreamFactory(function ($file, $mode) {
    return new Stream($file, $mode);
});

// Allow case insensitive urls
$app->add(function (\Slim\Http\Request $request, \Slim\Http\Response $response, callable $next) {

  $uri = $request->getUri();
  $uri = $uri->withPath(strtolower($uri->getPath()));
  $request = $request->withUri($uri);

  return $next($request, $response);
});

// Remove Trailling slashes
$app->add(Middleware::TrailingSlash(false)  //(optional) set true to add the trailing slash instead remove
              ->redirect(301));            //(optional) to return a 301 (seo friendly) or 302 response to the new path

// Odd behavior removing for now
// Minify HTML Response
// $app->add(Middleware::formatNegotiator());
// $app->add(Middleware::Minify());

// GZip Response
// Shouldn't need this should be handled by apache
// $app->add(Middleware::EncodingNegotiator(),Middleware::Gzip());
