window.onload=function() {

  function submitForm(){
    document.getElementById("form-search").submit();
  }

  var levelField = document.getElementById("level");
  levelField.addEventListener("change", submitForm);

  var typeField = document.getElementById("type");
  typeField.addEventListener("change", submitForm);

  var collegeField = document.getElementById("college");
  collegeField.addEventListener("change", submitForm);

  var pathwayField = document.getElementById("pathway");
  pathwayField.addEventListener("change", submitForm);

  var campusField = document.getElementById("campus");
  campusField.addEventListener("change", submitForm);

};
