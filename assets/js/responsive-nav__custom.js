// Init responsive nav
var customToggle = document.getElementById( 'nav-toggle' );
var navigation = responsiveNav(".nav-collapse", {
  animate: !0,
  insert: "before",
  transition: 600,
  customToggle: "#nav-toggle",
  init: function() {
      var el = document.getElementById('wvu-nav--menu-items');
      el.parentElement.innerHTML = el.innerHTML;
      responsiveNav(".nav-collapse", {
          animate: !0,
          insert: "before",
          transition: 600,
          customToggle: "#nav-toggle",
          enableFocus: !0,
          enableDropdown: !0,
          menuItems: "menu-items",
          subMenu: "sub-menu",
          openPos: "relative",
          openDropdown: '<span class="screen-reader-text">Open sub menu</span>',
          closeDropdown: '<span class="screen-reader-text">Close sub menu</span>',
          open: function() {
              customToggle.innerHTML = '<img src="https://static.wvu.edu/global/images/icons/wvu/hamburger-menu--1.0.0.svg" alt="" />Close Menu';
          },
          close: function() {
              customToggle.innerHTML = '<img src="https://static.wvu.edu/global/images/icons/wvu/hamburger-menu--1.0.0.svg" alt="" />Open Menu';
          },
          resizeMobile: function() {
              customToggle.setAttribute("aria-controls", "wvu-nav");
          },
          resizeDesktop: function() {
              customToggle.removeAttribute("aria-controls");
          }
      });
  },
  open: function () {
    customToggle.innerHTML = '<img src="https://static.wvu.edu/global/images/icons/wvu/hamburger-menu--1.0.0.svg" alt="" />Close Menu';
  },
  close: function () {
    customToggle.innerHTML = '<img src="https://static.wvu.edu/global/images/icons/wvu/hamburger-menu--1.0.0.svg" alt="" />Open Menu';
  },
  resizeMobile: function () {
    customToggle.setAttribute( 'aria-controls', 'wvu-nav' );
  },
  resizeDesktop: function () {
    customToggle.removeAttribute( 'aria-controls' );
  },
});
