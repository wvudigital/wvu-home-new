var fontA = new FontFaceObserver('HelveticaNeueW01-97Blac', {
  "family": "HelveticaNeueW01-97Blac",
  "weight": "normal"
});

var fontB = new FontFaceObserver('HelveticaNeueW01-45Ligh', {
  "family": "HelveticaNeueW01-45Ligh",
  "weight": "normal"
});

Promise.all([fontA.load(), fontB.load()]).then(function () {
  console.log('Fonts: HelveticaNeue Black & HelveticaNeue Light have loaded');
  document.documentElement.className += " fonts-loaded";
}).catch(err => {
  console.log("Font loading error, caught: ", err);
  document.documentElement.className += " fonts-unavailable";
});
