'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const pump = require('pump');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

// Sass task
// Compile Our Sass from the "scss" directory
gulp.task('sass', function (done) {
  gulp.src(['./assets/scss/*.scss', '!./assets/scss/_*.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('./public/css'));
  done();
});

gulp.task('scripts', function (done) {
  pump(
    gulp.src([
      './assets/js/wvu-program-filter-submit.js',
      './assets/js/responsive-tabs.js'
    ]),
    babel(),
    uglify(),
    gulp.dest('./public/javascripts/'),
    function (err) {
      if (err) {
        console.log(err);
      }
    }
  );

  pump(
    gulp.src([
      './node_modules/responsive-nav/responsive-nav.js',
      './assets/js/responsive-nav__custom.js',
      './node_modules/fontfaceobserver/fontfaceobserver.js'
      // NOTE: fontfaceobserver__custom.js is included as an inline script in the template & has Twig conditionals
    ]),
    babel(),
    uglify(),
    concat('all.js'),
    gulp.dest('./public/javascripts/'),
    function (err) {
      if (err) {
        console.log(err);
      }
    }
  );
  done();
});

gulp.task('default', ['sass', 'scripts'], function () {
  gulp.watch(['assets/scss/**/*.scss'], ['sass']);
  gulp.watch(['assets/js/**/*.js'], ['scripts']);
});
