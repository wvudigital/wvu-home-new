# -*- mode: ruby -*-
# vi: set ft=ruby :
# require 'pry-byebug'
Vagrant.configure('2') do |config|

  config.vm.synced_folder ".", "/srv/www/wvuhome/current", owner: "vagrant",  group: "www-data",  mount_options: ["dmode=775,fmode=664"]

  config.vm.define :development, primary: true do |conf|

    conf.vm.box      = 'ubuntu/bionic64'
    conf.vm.box_check_update = true

    conf.vm.hostname = 'web01.wvu.dev'

    conf.vm.network :private_network, ip: '192.168.56.10'

    # Forward Apache
    conf.vm.network :forwarded_port, guest: 80, host: 8888
    conf.vm.network :forwarded_port, guest: 8080, host: 8080

    conf.vm.provision "chef_solo" do |chef|
      chef.version = "15.7.32"
      chef.arguments = "--chef-license accept"

      chef.node_name = "application"
      chef.log_level = "info"
      chef.verbose_logging = true

      # Specify the local paths where Chef data is stored
      chef.cookbooks_path = ["provision/cookbooks/custom", "provision/cookbooks/vendor"]
      chef.data_bags_path = "provision/data_bags"
      chef.nodes_path = "provision/nodes"
      chef.roles_path = "provision/roles"
      chef.environment = "development"
      chef.environments_path = "provision/environments"
      # Add a role
      chef.add_role "appserver"

    end

    conf.vm.provider "virtualbox" do |v|
      v.memory = 1024
    end
  end
end
