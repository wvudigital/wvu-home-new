# syntax=docker/dockerfile:1

FROM ubuntu:18.04

RUN echo "America/New_York" | tee /etc/timezone

RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq \
  build-essential \
  ntp \
  wget \
  git \
  nano \
  unzip \
  php7.2 \
  php7.2-dev \
  php7.2-cli \
  php-pear \
  php7.2-xml \
  php7.2-mbstring \
  php7.2-gd \
  php7.2-curl \
  php7.2-zip \
  php-memcached \
  libmcrypt-dev \
  python-minimal \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN pecl channel-update pecl.php.net \
  && pecl update-channels \
  && printf "\n" | pecl install mcrypt-1.0.1

RUN wget https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-x64.tar.gz -O /tmp/node-v10.16.3-linux-x64.tar.gz \
  && cd /usr/local \
  && tar --strip-components 1 -xf /tmp/node-v10.16.3-linux-x64.tar.gz \
  && rm /tmp/node-v10.16.3-linux-x64.tar.gz

# Copies the path /usr/bin/composer from the composer:1.6 image
COPY --from=composer:1.6 /usr/bin/composer /usr/bin/composer

#RUN useradd -ms /bin/bash apprunner

# Setup app code
ENV INSTALL_PATH /srv/www
RUN mkdir -p $INSTALL_PATH 
#&& chown -R apprunner:apprunner $INSTALL_PATH

#USER apprunner

RUN mkdir -p /etc/ssh \
  && touch /etc/ssh/ssh_known_hosts \
  && ssh-keyscan github.com >> /etc/ssh/ssh_known_hosts \
  && ssh-keyscan bitbucket.org >> /etc/ssh/ssh_known_hosts

WORKDIR $INSTALL_PATH

COPY composer.json composer.json
COPY composer.lock composer.lock
RUN composer install

COPY package.json package.json
RUN npm install

CMD ["/bin/bash"] 
