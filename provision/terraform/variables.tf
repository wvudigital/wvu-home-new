variable "environment" {
  type = "string"
  default = "staging"
}

variable "aws_region" {
  type = "string"
  default = ""
}

variable "aws_s3_bucket" {
  type = "string"
  default = ""
}

variable "aws_az" {
  type = "list"
}

variable "aws_ami" {
  type = "string"
  default = ""
}

variable "aws_vpc_id" {
  type = "string"
}

variable "aws_instance_type" {
  type = "string"
  default = "t2.small"
}

variable "aws_access_key" {
  type = "string"
  default = ""
}

variable "aws_secret_access_key" {
  type = "string"
  default = ""
}

variable "aws_public_ip" {
  type = "string"
  default = "true"
}

variable "aws_key_name" {
  type = "string"
  default = ""
}

variable "server_count" {
  default = 2
}

variable "vpc_security_group_ids" {
  type = "list"
}

variable "stack" {
  type = "string"
  default = "www"
}

variable "aws_alb_target_group_arn" {
  type = "string"
}

variable "aws_alb_target_group_port" {
  type = "string"
}

variable "aws_volume_type" {
  type = "string"
}

variable "aws_volume_size" {
  type = "string"
}
