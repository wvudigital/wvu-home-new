# Add test config here

# This file is pretty much the standard base.tf however the test env does not have
# a load balancer so does not need to have that configuration


# Setup AWS provider
provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_access_key}"
  region     = "${var.aws_region}"
}

resource "aws_s3_bucket" "terraform-state-storage-s3" {
  bucket = "${var.aws_s3_bucket}"
  tags = {
    Name = "S3 Remote Terraform State Store"
  }
  versioning {
    enabled = true
  }
  lifecycle {
    prevent_destroy = false
  }
}

# Setup System Manager role
resource "aws_iam_instance_profile" "www-profile" {
  name = "${var.stack}-profile-${var.environment}"
  role = "www2-systems-manager"
}


data "aws_subnet_ids" "web" {
  vpc_id = "${var.aws_vpc_id}"
  tags = {
    Supports = "T3"
  }
}

resource "random_shuffle" "az" {
  input = "${data.aws_subnet_ids.web.ids}"
}


# Create AWS Instances
resource "aws_instance" "web" {
  count                         = "${var.server_count}"
  ami                           = "${var.aws_ami}"
  instance_type                 = "${var.aws_instance_type}"
  associate_public_ip_address   = "${var.aws_public_ip}"
  subnet_id                     = "${element(random_shuffle.az.result, count.index)}"
  key_name                      = "${var.aws_key_name}"
  vpc_security_group_ids        = "${var.vpc_security_group_ids}"
  iam_instance_profile          = "${aws_iam_instance_profile.www-profile.id}"
  tags                          = {
                                    Name = "${format("${var.stack}-${var.environment}-%d", count.index + 1)}",
                                    Env = "${var.environment}",
                                    Stack = "${var.stack}"
                                    Packer = "yes"
                                  }

  root_block_device {
    volume_type                 = "${var.aws_volume_type}"
    volume_size                 = "${var.aws_volume_size}"
  }

}
