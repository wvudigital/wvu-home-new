terraform {
 backend "s3" {
   bucket = "www-statefiles"
   region = "us-east-1"
   key = "production/packer/terraform.tfstate"
 }
}

# Attach Instances to Application Load Balancer
# resource "aws_alb_target_group_attachment" "web" {
#   count = "${var.count}"
#   target_group_arn = "${var.aws_alb_target_group_arn}"
#   target_id = "${aws_instance.web.*.id[count.index]}"
#   port = "${var.aws_alb_target_group_port}"
# }
