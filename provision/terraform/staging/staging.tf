terraform {
 backend "s3" {
   bucket = "www-statefiles-test"
   region = "us-east-1"
   key = "staging/packer/terraform.tfstate"
 }
}

# Creating a ALB attachment has a bug, can't seperate the instances on single destroy.
# Attach Instances to Application Load Balancer
# resource "aws_alb_target_group_attachment" "web" {
#   count = "${var.count}"
#   target_group_arn = "${var.aws_alb_target_group_arn}"
#   # target_id = "${element(aws_instance.web.*.id, count.index)}"
#   target_id = "${aws_instance.web.*.id[count.index]}"
#   port = "${var.aws_alb_target_group_port}"
# }
