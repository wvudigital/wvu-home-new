name 'php-server'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures php-server'
long_description 'Installs/Configures php-server'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/php-server/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/php-server'

depends 'ntp', '~> 3.7.0'
depends 'apache2', '~> 7.1.1'
depends 'php', '~> 7.0.0'
depends 'nodejs', '~> 6.0.0'
depends 'composer', '~> 2.6.1'
depends 'logrotate', '~> 2.2.2'
depends 'aws', '~> 8.0.4'
depends 'users', '~> 5.4.0'
depends 'ssh', '~> 0.10.24'
depends 'memcached', '~> 5.1.1'
depends 'sturdy_ssm_agent', '~> 2.1.0'
depends 'newrelic', '2.40.3'
