module WebApp
  module Helper
    def apache_dir
      case node['platform_family']
      when 'debian', 'suse'
        '/etc/apache2'
      when 'freebsd'
        '/usr/local/etc/apache24'
      else
        '/etc/httpd'
      end
    end

    def log_dir
      case node['platform_family']
      when 'debian', 'suse'
        '/var/log/apache2'
      when 'freebsd'
        '/var/log'
      else
        '/var/log/httpd'
      end
    end
  end
end
