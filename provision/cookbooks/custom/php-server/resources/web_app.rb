resource_name :web_app

property :template, String,
         default: 'web_app.conf.erb',
         description: 'Template name'
property :cookbook, String,
         default: 'php-server',
         description: 'Cookbok to source the template from'
property :local, [true, false],
          default: false,
          description: 'Load a template from a local path. By default, the chef-client
loads templates from a cookbook’s /templates directory. When this property is
set to true, use the source property to specify the path to a template on the
local node.'
property :enable, [true, false],
         default: true,
         description: 'enable or disable the site'
property :server_port, Integer,
         default: 80,
         description: 'Port to listen on'
property :root_group, String,
          default: node['platform_family'] == 'freebsd' ? 'wheel' : 'root',
          description: ''

property :parameters, Hash,
          description: ''

action :enable do

  apache2_module 'rewrite'

  apache2_module 'deflate'

  apache2_module 'headers' do
    apache_service_notification :restart
  end

  # puts "##########################################################"
  # puts new_resource.name

  template "#{apache_dir}/sites-available/#{new_resource.name}.conf" do
    source new_resource.template
    cookbook new_resource.cookbook
    local new_resource.local
    owner 'root'
    group new_resource.root_group
    mode '0644'
    variables(
      name: new_resource.name,
      params: new_resource.parameters,
      log_dir: log_dir
    )
    if ::File.exist?("#{apache_dir}/sites-enabled/#{new_resource.name}.conf")
      notifies :reload, 'service[apache2]', :delayed
    end
  end

end

action_class do
  include WebApp::Helper
end
