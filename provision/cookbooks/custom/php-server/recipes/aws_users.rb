chef_gem "aws-sdk-iam" do
  compile_time true
  action :install
end


require 'aws-sdk-iam'

credentials = data_bag_item('aws', "credentials")
iam_client = Aws::IAM::Client.new(
  region: node[:aws][:region],
  access_key_id: credentials['access_key'],
  secret_access_key: credentials['secret_key']
)
admins = iam_client.get_group(group_name: 'Admins').users
admin_users = []
admins.each_with_index do |user,index|

  admin_users[index] = {}
  admin_users[index]['user'] = user

  user_public_keys = iam_client.list_ssh_public_keys(user_name: user[:user_name])
  user_active_public_key_ids = user_public_keys[:ssh_public_keys].collect do |k|
    if k.status == "Active"
      k[:ssh_public_key_id]
    end
  end

  admin_users[index]['public_keys'] = []
  user_active_public_key_ids.each do |public_key_id|
     public_key = []
     public_key << iam_client.get_ssh_public_key(user_name: user[:user_name], ssh_public_key_id: "#{public_key_id}", encoding: 'SSH')
     admin_users[index]['public_keys'] << public_key.first[:ssh_public_key]
  end

end

admin_users.each_with_index do |user|

  user_name = user['user'].user_name
  user user_name do
    manage_home true
    shell '/bin/bash'
    action :create
  end

  directory "/home/#{user_name}/.ssh/" do
    mode '0700'
    owner user_name
    group user_name
    not_if { ::File.directory?("/home/#{user_name}/.ssh/") }
  end

  keys = []
  user['public_keys'].each do |key|
    keys << key[:ssh_public_key_body]
  end
  keys = keys.join("\n")

  file "/home/#{user_name}/.ssh/authorized_keys" do
    content keys
    mode '0600'
    owner user_name
    group user_name
  end

  sudo user_name do
    users user_name
    nopasswd true
  end

end

execute "checkout aws_users repo" do
  command "git clone #{node['php-server'][:aws_users_git][:repo]} --single-branch --branch #{node['php-server'][:aws_users_git][:branch]} /home/#{node['php-server'][:deploy_user]}/aws_users"
  user node['php-server'][:deploy_user]
  environment ({'HOME' => "/home/#{node['php-server'][:deploy_user]}", 'USER' => "#{node['php-server'][:deploy_user]}"})
end

execute "move aws_users dir" do
  command "mv /home/#{node['php-server'][:deploy_user]}/aws_users /opt/aws_user"
  user "root"
end

# Fix permisions
execute "change ownership of aws_user dir" do
  command "chown -R root:root /opt/aws_user"
  user "root"
end

# build rc.local file

file "/etc/rc.local" do
  content "chef-client --local -c /opt/aws_users/solo.rb -o 'recipe[aws_user]'\n\nexit 0"
  mode '0700'
  owner "root"
  group "root"
end
