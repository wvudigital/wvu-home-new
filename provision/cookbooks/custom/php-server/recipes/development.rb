# After configs have been copied install libapache2-mod-php
package "libapache2-mod-php"

# restart apache
service 'apache2' do
  action 'restart'
end

web_app node['php-server'][:app_name] do
  parameters(
    docroot: node['php-server'][:doc_root],
    server_name: node['php-server'][:domain],
    server_aliases: node['php-server'][:hostnames],
    server_port: node['php-server'][:port] || 80
  )
end

apache2_site node['php-server'][:app_name]

apache2_default_site 'disable default' do
  default_site_name '000-default'
  action :disable
end

apache2_default_site 'enable zzz-default' do
  default_site_name 'zzz-default'
  template_source 'default-site.conf.erb'
  action :enable
end


# Install memcached
memcached_instance 'memcached' do
  port 11211
  memory 64
  listen '0.0.0.0'
end

# Install composer packages for app
composer_project node['php-server'][:app_path]+'/current' do
  quiet false
  user node['php-server'][:deploy_user]
  group node['php-server'][:deploy_group]
  action :install
end

# Create and set permissions on cache directory
directory node['php-server'][:app_path] + "/current/templates/cache" do
  owner 'www-data'
  group 'root'
  mode 0775
  action :create
end
