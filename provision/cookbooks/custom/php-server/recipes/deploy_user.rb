Chef::Log.info("Creating user from #{node['php-server'][:deploy_user_databag]}")

users_manage node['php-server']['deploy_user'] do
  group_id 1138
  action [:create]
  data_bag node['php-server'][:deploy_user_databag]
end

# Create .ssh config
ssh_config "bitbucket.org" do
  options "Hostname"=>"bitbucket.org",
          "User"=>"git",
          "PreferredAuthentications"=>"publickey",
          "IdentityFile"=> "/home/#{node['php-server'][:deploy_user]}/.ssh/id_rsa",
          "AddKeysToAgent"=> "yes"
  user node['php-server'][:deploy_user]
end
