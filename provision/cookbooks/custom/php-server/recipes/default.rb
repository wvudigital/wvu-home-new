#
# Cookbook:: php-server
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.


# Update APT
apt_update 'update'

# Install Build Essential
build_essential do
  compile_time false
end

# Set Timezone to EST
timezone 'name' do
  timezone 'America/New_York'
end

# Install NTP
include_recipe 'ntp'

# Install Logrotate
include_recipe 'logrotate::default'

# Install Apache
apache2_install 'default_install'

# Set Apache Service
service 'apache2' do
  extend Apache2::Cookbook::Helpers
  service_name lazy { apache_platform_service_name }
  supports restart: true, status: true, reload: true
  action :nothing
end

execute "a2dismod php7.2" do
  command "a2dismod php7.2"
  notifies :restart, 'service[apache2]', :immediately
  ignore_failure true
  only_if { ::File.symlink?("/etc/apache2/mods-enabled/php7.2.load")}
end

# Override apache2.conf with an extra custom config
apache2_conf 'custom-apache2' do
  action :enable
  template_cookbook "php-server"
end

# Install PHP
include_recipe 'php'

# Install mcrypt pecl
php_pear 'mcrypt-1.0.1' do
  version '1.0.1'
  package_name 'mcrypt'
  zend_extensions ['mcrypt.so']
  channel 'pecl.php.net'
end

# Install NodeJS
include_recipe 'nodejs'

# Install Git
package 'git'


# Add bitbucket to known_hosts
ssh_known_hosts_entry 'name' do
  host "bitbucket.org"
  action :create
end

# Install Composer
include_recipe 'composer::install'
