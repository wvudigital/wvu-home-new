include_recipe 'newrelic'

node.default['newrelic']['license'] = data_bag_item('newrelic', "credentials")[node.chef_environment]['license']
# Install New Relic
newrelic_agent_php 'Install' do
  enable_module true
  app_name 'WVU Homepage'
  service_name 'apache2'
  config_file '/etc/php/7.2/mods-available/newrelic.ini'
  config_file_to_be_deleted '/etc/php/7.2/cli/conf.d/newrelic.ini'
  action :install
end
