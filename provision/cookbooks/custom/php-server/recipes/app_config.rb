# Build App directory recursively creates full path
directory "#{node['php-server'][:app_path]}/shared/logs" do
  owner node['php-server'][:deploy_user]
  group node['php-server'][:deploy_group]
  mode 0755
  recursive true
end

# Directory recursive true only applies group owner and mode to the leaf Directory
# lets fix that by applying correct permissions directly on the parent dir.
execute "chown_to_deploy_user" do
  command "chown -R #{node['php-server'][:deploy_user]}:#{node['php-server'][:deploy_group]} #{node['php-server'][:app_path]}"
  user "root"
  action :run
end

# Add inital app.log
file node['php-server'][:app_path] + "/shared/logs/app.log" do
  owner 'www-data'
  group 'root'
  mode 0755
end

# Setup log rotate for app.log
logrotate_app node['php-server'][:app_name] do
  path      node['php-server'][:app_path] + "/shared/logs/*.log"
  frequency 'daily'
  create    '644 root root'
  rotate    7
end

credentials = data_bag_item('aws', "credentials")
# Get ENV file from S3
aws_s3_file node['php-server'][:app_path] + "/shared/.env" do
  bucket node['aws']['s3']['bucket']
  remote_path node['aws']['s3']['env_file']
  region node['aws']['region']
  aws_access_key credentials['access_key']
  aws_secret_access_key credentials['secret_key']
end

# Create and set permissions on cache directory
directory node['php-server'][:app_path] + "/shared/cache" do
  owner 'www-data'
  group 'root'
  mode 0775
  action :create
end

# checkout update_app_env repo for systems manager
execute "checkout update_app_env repo" do
  command "git clone #{node['php-server'][:update_app_env_git][:repo]} --single-branch --branch #{node['php-server'][:update_app_env_git][:branch]} /home/#{node['php-server'][:deploy_user]}/update_app_env"
  user node['php-server'][:deploy_user]
  environment ({'HOME' => "/home/#{node['php-server'][:deploy_user]}", 'USER' => "#{node['php-server'][:deploy_user]}"})
end

execute "move update_app_env dir" do
  command "mv /home/#{node['php-server'][:deploy_user]}/update_app_env /opt/update_app_env"
  user "root"
end

# Fix permisions
execute "change ownership of update_app_env dir" do
  command "chown -R root:root /opt/update_app_env"
  user "root"
end

# Enable newrelic
execute "phpenmod newrelic" do
  user "root"
  command "phpenmod newrelic"
  notifies :restart, 'service[apache2]', :immediately
end
