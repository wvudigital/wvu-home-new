execute "Remove default apache config" do
  command "rm -rf /etc/apache2"
end

directory "/etc/apache2" do
  user node['php-server'][:deploy_user]
  group node['php-server'][:deploy_group]
end

# Couldn't get repo checkout to work
# git "checkout repo" do
#   repository node['php-server'][:apache_config_git][:repo]
#   depth 1
#   checkout_branch node['php-server'][:apache_config_git][:branch]
#   destination "/etc/apache2"
#   notifies :restart, 'service[apache2]', :delayed
#   ssh_wrapper "ssh -i /home/#{node['php-server'][:deploy_user]}/.ssh/id_rsa -o StrictHostKeyChecking=no -o IdentitiesOnly=yes"
#   user node['php-server'][:deploy_user]
#   group node['php-server'][:deploy_group]
#   action :checkout
# end

execute "config git user.email" do
  command "git config --global user.email 'wvu.webservices@gmail.com'"
  user node['php-server'][:deploy_user]
  environment ({'HOME' => "/home/#{node['php-server'][:deploy_user]}", 'USER' => "#{node['php-server'][:deploy_user]}"})
end

execute "config git user.name" do
  command "git config --global user.name 'WVU Digital'"
  user node['php-server'][:deploy_user]
  environment ({'HOME' => "/home/#{node['php-server'][:deploy_user]}", 'USER' => "#{node['php-server'][:deploy_user]}"})
end

# Add known_host config
ssh_known_hosts_entry "bitbucket.org" do
  file_location "/home/#{node['php-server'][:deploy_user]}/.ssh/known_hosts"
  hash_entries true
  owner node['php-server'][:deploy_user]
  group node['php-server'][:deploy_user]
end

execute "checkout apache config repo" do
  command "git clone #{node['php-server'][:apache_config_git][:repo]} --single-branch --branch #{node['php-server'][:apache_config_git][:branch]} /etc/apache2"
  user node['php-server'][:deploy_user]
  environment ({'HOME' => "/home/#{node['php-server'][:deploy_user]}", 'USER' => "#{node['php-server'][:deploy_user]}"})
end

# Fix permisions back to
execute "change ownership of apache config dir" do
  command "chown -R root:www-data /etc/apache2"
  user "root"
end

# Reinitalize apache modules
modules = %w(mpm_prefork authz_core authz_host access_compat ssl headers alias auth_basic authn_core autoindex deflate dir env filter negotiation rewrite status mime setenvif)
modules.each do |mod|
  apache2_module mod do
    action :enable
  end
end

# After configs have been copied install libapache2-mod-php
package "libapache2-mod-php"

# restart apache
service 'apache2' do
  action 'restart'
end


# checkout apache_config_update repo for systems manager
execute "checkout aws_users repo" do
  command "git clone #{node['php-server'][:apache_config_update_git][:repo]} --single-branch --branch #{node['php-server'][:apache_config_update_git][:branch]} /home/#{node['php-server'][:deploy_user]}/aws_users"
  user node['php-server'][:deploy_user]
  environment ({'HOME' => "/home/#{node['php-server'][:deploy_user]}", 'USER' => "#{node['php-server'][:deploy_user]}"})
end

execute "move apache_config_update dir" do
  command "mv /home/#{node['php-server'][:deploy_user]}/aws_users /opt/apache_config_update"
  user "root"
end

# Fix permisions
execute "change ownership of apache_config_update dir" do
  command "chown -R root:root /opt/apache_config_update"
  user "root"
end
