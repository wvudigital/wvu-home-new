# Mount EFS Drive
package 'Install NFS client' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name 'nfs-utils'
  when 'ubuntu', 'debian'
    package_name 'nfs-common'
  end
end

directory node['aws']['efs']['mount_point'] do
  owner node['aws']['efs']['user']
  group node['aws']['efs']['group']
  mode '0755'
  recursive true
end

mount 'Mount EFS' do
  device node['aws']['efs']['dns']
  fstype node['aws']['efs']['fstype']
  options node['aws']['efs']['options']
  ignore_failure node['aws']['efs']['ignore_failure']
  action [:mount, :enable]
end


# Install SSM Agent
# snap_package 'Install Amazon SSM' do
#   options "--classic"
#   timeout 5
#   package_name "amazon-ssm-agent"
#   ignore_failure true
# end
#
# service "amazon-ssm-agent" do
#   action [:disable, :start]
# end

# Include sturdy_ssm_agent instead of trying manual install
include_recipe 'sturdy_ssm_agent::default'
