# Install AWS CLI
apt_package "python-pip" do
  action :install
end

execute "upgrade pip" do
  command "pip install --upgrade pip"
  action :run
end

execute "install awscli" do
  command "pip install awscli"
  action :run
end

# Add AWS CONFIG
directory "/home/"+node[:aws][:config][:user]+"/.aws/" do
  owner node[:aws][:config][:user]
  group node[:aws][:config][:group]
  mode '0600'
  recursive true
end

template "/home/"+node[:aws][:config][:user]+"/.aws/config" do
  source 'aws_config.txt.erb'
  owner node[:aws][:config][:user]
  group node[:aws][:config][:group]
  mode '0600'
  variables(
    region: node['aws']['region']
  )
end

credentials = data_bag_item('aws', "credentials")
template "/home/"+node[:aws][:config][:user]+"/.aws/credentials" do
  source 'aws_credentials.txt.erb'
  owner node[:aws][:config][:user]
  group node[:aws][:config][:group]
  mode '0600'
  variables(
    access_key: credentials['access_key'],
    secret_key: credentials['secret_key']
  )
end
