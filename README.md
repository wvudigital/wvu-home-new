# WVU Homepage

The home page for [West Virginia University](http://www.wvu.edu)

For setup see Local Development installation section below.

## Last Deploy Status

Staging:
[![Deployment status from DeployBot](https://wvu.deploybot.com/badge/23779030051275/122067.svg)](http://deploybot.com)

Production:
[![Deployment status from DeployBot](https://wvu.deploybot.com/badge/02267418030765/124101.svg)](http://deploybot.com)

## Frontend Development installation instructions (Docker Desktop)

This is the new way. It has been tested on macOS Sonoma. Make sure you have updated [XCode](https://developer.apple.com/xcode/).

### Front end tooling installation

1. Download and install [Docker Desktop](https://www.docker.com/products/docker-desktop/). Make sure you select the correct Mac processor option.
1. Download [https://wvu-homepage.s3.amazonaws.com/wvu-home-php_intel_dev_docker_image_20240131.tar.bz2](https://wvu-homepage.s3.amazonaws.com/wvu-home-php_intel_dev_docker_image_20240131.tar.bz2)
1. Checkout the `staging` branch of the repo.
1. Make sure Docker Desktop is running.
1. Run `docker load -i the/path/to/the/file-you-downloaded.tar.bz2`
1. Run `docker compose up`.
1. You should be able to visit `localhost:8080` in your browser to see the site.

### Deploy instructions

1. **NEVER MAKE DIRECT CHANGES IN `master` BRANCH**
1. See front end tooling for more info above.
1. See step 1.
1. Checkout `staging` branch `git checkout staging`
1. Pull the latest changes `git pull`
1. From your project root, run Docker Desktop `docker compose up`
1. Make changes to the site content
1. View and validate changes at: `http://localhost:8080`
1. Add all modified files to git stage `git add --all`
1. Commit changes `git commit -m "descriptive message of the change"`
1. Push changes `git push origin staging`
1. Review changes https://www.volutus.wvu.edu
1. Stash any uncommitted changes `git stash`
1. Switch to `master` branch `git checkout master`
1. Rebase `master` with `staging` changes `git rebase staging`
1. Push changes `git push origin master`
1. Review changes https://www.wvu.edu
1. **IMPORTANT:** Switch back to `staging` branch `git checkout staging`
1. Unstash any uncommitted changes `git stash apply`

## Frontend Development installation instructions (VirtualBox)

This is the old way.

1. Install [VirtualBox](https://www.virtualbox.org) at version 6.0.x
1. Install [Vagrant](http://vagrantup.com) at version 2.2.x
1. Install git hooks, node, gulp, chef, chef-recipes, and all other package dependencies, listed under Front end tooling installation below.
1. Make sure to have `.env` file in the root of project directory.
1. Build the dev environment with `vagrant up`
1. (Optional) To SSH into development box use `vagrant ssh`
1. (Optional) To stop the dev environment `vagrant halt`
1. (Optional) To destroy the dev environment  `vagrant destroy`

### Front end tooling installation

#### Git Pre-Hook Installation

1. Create the file `.git/hooks/pre-commit` with following content:

```bash
BRANCH=`git rev-parse --abbrev-ref HEAD`

if [[ "$BRANCH" == "master" ]]; then
  echo "You are on branch $BRANCH. This is not recommended. Are you sure you want to commit to this branch?"
  echo "If so, commit with -n to bypass this pre-commit hook."
  exit 1
fi

exit 0
```
2. Make it executable

```bash
sudo chmod +x .git/hooks/pre-commit
```

#### Chef Installation
* Install latest ChefDK package from  [https://downloads.chef.io/chefdk/](https://downloads.chef.io/chefdk/) for your operating system.

##### Install Chef Recipes

* Make sure ChefDK is installed
* From root of project folder:
* `cd provision/cookbooks/custom/php-server`
* `berks install`  # installs dependencies on machine but not in app folder
* `berks vendor ../../vendor`  # installs dependencies recipes into `provision/cookbooks/vendor/*`

#### NodeJS Installation

* To check if NodeJS already installed:  type `node --version` in console
* Otherwise, visit [http://nodejs.org/](http://nodejs.org/) and download the latest package and install it

#### Gulp Installation

* To check if Gulp is already installed type `gulp --version` in console.
* Otherwise, `npm install -g gulp`

#### Install all node packages and dependencies

* From application directory run `npm install` to install all the development dependencies for gulp.

#### Run Gulp

* Run `gulp` this will fire up a gulp server giving you to browserSync reloading while compiling sass, compressing and concatenating all Javascripts.

### Deploy instructions

1. **NEVER MAKE DIRECT CHANGES IN `master` BRANCH**
1. Install git pre hook to stop this from happening see front end tooling for more info above.
1. See step 1.
1. Checkout `staging` branch `git checkout staging`
1. Pull the latest changes `git pull`
1. Load Vagrant VM `vagrant up`
1. Run gulp `gulp`, gulp will auto open a browser window for you.
1. Make changes to the site content
1. View and validate changes at: `http://www.lvh.wvu.edu:8888`
1. Add all modified files to git stage `git add --all`
1. Commit changes `git commit -m "descriptive message of the change"`
1. Push changes `git push origin staging`
1. Review changes https://www.volutus.wvu.edu
1. Stash any uncommitted changes `git stash`
1. Switch to `master` branch `git checkout master`
1. Rebase `master` with `staging` changes `git rebase staging`
1. Push changes `git push origin master`
1. Review changes https://www.wvu.edu
1. **IMPORTANT:** Switch back to `staging` branch `git checkout staging`
1. Unstash any uncommitted changes `git stash apply`

#### Common Issues

1. **Incorrect (old) timed content is showing instead of current content in my local instance.** Timed content is displayed based on the current time of the server. In this case, the local Vagrant box. It can get out of sync with real time. `vagrant halt` and `vagrant up` to get back on the right time.

---

## Backend Server Development

### Packer AMI Provisioning

1. Install [Packer](https://packer.io) at version 1.5.x
1. Run a build to create a new AMI:

```
packer build -only=amazon-ebs -force -on-error=ask \
-var 'aws_access_key={your aws access key}' \
-var 'aws_secret_key={you aws secret key}' \
-var 'environment={environment}' packer.json
```


### Terraform Setup

1. Install [Terraform](https://www.terraform.io/) at version 0.12.9
1. Create a `staging.tfvars.json` file in `/provision/terraform/staging` using the variable definitions in `variables.tf`
1. Create a `production.tfvars.json` file in `/provision/terraform/production` using the variable definitions in `variables.tf`


*.tfvars.json example:

```json
{
"server_count": 1,
"environment": "",
"stack": "",
"aws_ami": "",
"aws_instance_type": "",
"aws_volume_type": "",
"aws_volume_size": "",
"aws_region": "",
"aws_s3_bucket": "",
"aws_az": [""],
"aws_access_key": "",
"aws_secret_access_key": "",
"aws_key_name": "",
"vpc_security_group_ids": [""],
"aws_alb_target_group_arn": "",
"aws_alb_target_group_port": ""
}
```

### Staging Infrastructure Provisioning

1. NEVER MAKE DIRECT CHANGES IN `master` BRANCH
1. If you installed your git pre hook you dont need to worry about this.
1. See step 1.
1. Check out staging `git checkout staging`
1. Create new AMI with packer with staging environment
1. Add configuration variables in a `staging.tfvars` file in `/provision/staging` including new AMI id
1. To create the environment from `/provision/terraform/staging` run  `terraform apply -var-file=staging.tfvars`
1. To destroy the environment run `terraform destroy -var-file=staging.tfvars`
1. Add configuration changes to DeployBot.
1. Commit changes and push staging. `git add -A && git commit -m 'some message' && git push`
1. Redeploy site code from DeployBot using full redeploy.

*Tip*: To add or remove additional EC2 instances from the plan, change the value of count in `staging.tfvars`

#### Adding or Destroy single staging instances.
1. Change directory to `cd ~/{projectroot}/provision/terraform/staging`
1. NEVER MAKE DIRECT CHANGES IN `master` BRANCH
1. If you installed your git pre hook you dont need to worry about this.
1. See step 2.
1. Check out staging `git checkout staging`
1. Get the "number" of the server you want to destroy.  This is a 0 index start.  If there are 2 servers, 0 would be www-staging-1, and 1 would be www-staging-2 and so forth.
1. You will need to modify both resources: instance and the alb `-target aws_instance.web[{number}]` and `-target aws_alb_target_group_attachement.web[{number}]` respectively.
  1. To **build** a new server `terraform apply -var-file=staging.tfvars -target aws_instance.web[{number}]`
  1. To **preview** a change `terraform plan -var-file=staging.tfvars -target aws_instance.web[{number}]`  
  1. To **destroy** a currently available server `terraform destroy -var-file=staging.tfvars -target aws_instance.web[{number}]`
  1. To **preview** a destroy `terraform plan --destroy -var-file=staging.tfvars -target aws_instance.web[{number}]`
1. Add configuration changes to DeployBot.
1. Commit changes and push staging. `git add -A && git commit -m 'some message' && git push`
1. Redeploy site code from DeployBot using full redeploy on new server.

---

### Production Infrastructure Provisioning

1. NEVER MAKE DIRECT CHANGES IN `master` BRANCH
1. If you installed your git pre hook you dont need to worry about this.
1. See step 1.
1. Check out staging `git checkout staging`
1. Create new AMI with packer with production environment
1. Add configuration variables in a `production.tfvars` file in `/provision/production` including new AMI id
1. To create the environment from `/provision/terraform/production` run  `terraform apply -var-file=production.tfvars`
1. To destroy the environment run `terraform destroy -var-file=production.tfvars`
1. Commit changes to staging `git add -A && git commit -m 'some message' && git push`
1. Rebase master and push `git checkout master && git rebase staging && git push`
1. Add configuration changes to DeployBot.
1. Redeploy site code from DeployBot using full redeploy.
1. Switch back to staging. `git checkout staging`

*Tip*: To add or remove additional EC2 instances from the plan, change the value of count in `production.tfvars`

#### Adding or Destroy single production instances.
1. Change directory to `cd ~/{projectroot}/provision/terraform/production`
1. NEVER MAKE DIRECT CHANGES IN `master` BRANCH
1. If you installed your git pre hook you dont need to worry about this.
1. See step 2.
1. Check out staging `git checkout staging`
1. Get the "number" of the server you want to destroy.  This is a 0 index start.  If there are 2 servers, 0 would be www-staging-1, and 1 would be www-staging-2 and so forth.
1. You will need to modify both resources: instance and the alb `-target aws_instance.web[{number}]` and `-target aws_alb_target_group_attachement.web[{number}]` respectively.
  1. To **destroy** a currently available server `terraform destroy -var-file=production.tfvars -target aws_instance.web[{number}]`
  1. To **build** a new server `terraform apply -var-file=production.tfvars -target aws_instance.web[{number}]`
  1. To **preview** a change `terraform plan -var-file=production.tfvars -target aws_instance.web[{number}]`
  1. To **preview** a destroy `terraform plan --destroy -var-file=production.tfvars -target aws_instance.web[{number}]`
1. Commit changes to staging `git add -A && git commit -m 'some message' && git push`
1. Rebase master and push `git checkout master && git rebase staging && git push`
1. Add configuration changes to DeployBot.
1. Redeploy site code from DeployBot using full redeploy on new server.
1. Switch back to staging. `git checkout staging`

---

### Developer Notes

##### What's Happening

1. Your SSH key needs to be added to your IAM user.  The homepage EC2's will need a reboot in order to access these new SSH Keys.  There appears to be a bit of lag time on adding a SSH key and the EC2 reboot pulling the new Key.  Wait 5mins after adding the key then reboot each EC2.  You should then be able to login.
1. The What's happening application has been rewritten as a Ruby on Rails application, the homepage needs the ability to access the article list JSON url.
The security group for the Ruby on Rails server needs to provide access from the homepage server IP addresses in the `staging` environment.


---

### BETA ENV

** Reminder Note **
Beta Environment does not set itself up on provision.  

Things to do manually after a provision:

1. Setup application directory
1. Deploybot key needs manually added to deploy user.

test
