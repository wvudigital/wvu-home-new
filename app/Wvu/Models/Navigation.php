<?php

namespace Wvu\Models;

use Symfony\Component\Yaml\Yaml;

class Navigation {

  /**
  * Initialize the app var
  */
  public function __construct() {
    try {
      $this->data = Yaml::parseFile(__DIR__.'/../../../data/navigation.yml');
    } catch (ParseException $e) {
      printf('Unable to parse the YAML string: %s', $e->getMessage());
    }
  }
}
