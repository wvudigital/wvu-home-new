<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;

class SiteIndexController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $siteindex = new Services\SiteIndex($_ENV['SITEINDEX_JSON_URL'],'siteindex');

    $route = $request->getAttribute('route');
    $page = $request->getUri()->getPath();

    $this->getBreadcrumbs($this->data['navigation']['main'], $page, $breadcrumbs);
    $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

    $this->data['contentful'] = [ 'title' => 'A-Z Index' ];
    $this->data['siteindex'] = $siteindex->getCache();
    $this->data['currentPage'] = $request->getUri()->getPath();

    return $this->data;

  }

}
