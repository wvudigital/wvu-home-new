<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;

class SiteMapController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $route = $request->getAttribute('route');
    $page = $request->getUri()->getPath();

    $this->getBreadcrumbs($this->data['navigation']['main'], $page, $breadcrumbs);
    $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

    $this->data['contentful'] = [ 'title' => 'Site Map' ];
    $this->data['currentPage'] = $request->getUri()->getPath();

    return $this->data;

  }

}
