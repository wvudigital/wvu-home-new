<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;
use Wvu\Services\Wvutoday;
use Wvu\Services\Calendar;

class HomeController extends Controller {

  function __construct() {

    parent::__construct();

    $wvutoday = new Wvutoday($_ENV['WVUTODAY_RSS_URL'], 'wvutoday');
    $calendar = new Calendar($_ENV['CALENDAR_XML_URL'], 'calendar');

    $this->data['calendar'] = $calendar->getCache();
    $this->data['wvutoday'] = $wvutoday->getCache();

  }

  public function view($request) {

    $contentful = new Services\Contentful($request);

    $this->data['currentPage'] = $request->getUri()->getPath();
    // Contentful needs to be called last, otherwise data wont be passed to it.
    $this->data['contentful'] = $contentful->getData('home', $this->data)['contentful'];

    $this->data['inline_css'] = [
      'status' => $_ENV['INLINE_CSS'] === 'true' ? true : false,
      'contents' => file_get_contents(__DIR__."/../../../public/css/inline.css")
    ];

    return $this->data;

  }

}
