<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;

class CourseController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $contentful = new Services\ContentfulCourses($request);

    $route = $request->getAttribute('route');
    $page = $route->getArgument('page');

    $this->getBreadcrumbs($this->data['navigation']['main'], "/academics/courses", $breadcrumbs);
    $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

    $this->data['queryParams'] = $request->getQueryParams();
    $this->data['currentPage'] = $request->getUri()->getPath();

    // Contentful needs to be called last, otherwise data wont be passed to it.

    $this->data['course'] = $contentful->getData($page, $this->data)['contentful'];
    $this->data['contentful'] = array('title' => $this->data['course']['courseSubjectAndNumber'].": ".$this->data['course']['title']);
    $this->data['breadcrumbs'][] = array("title" => "Academics", "path" => "/academics");
    $this->data['breadcrumbs'][] = array("title" => $this->data['course']['courseSubjectAndNumber'].": ".$this->data['course']['title']);

    return $this->data;

  }
}
