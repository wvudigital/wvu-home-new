<?php

namespace Wvu\Controllers;

use Wvu\Controllers;

class ErrorController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $route = $request->getAttribute('route');
    $page = $route->getArgument('page');

    $this->data['page'] = [ 'title' => 'Error', 'route' => $page ];
    $this->data['template'] = 'error.html';
    $this->data['currentPage'] = $request->getUri()->getPath();

    return $this->data;

  }
}
