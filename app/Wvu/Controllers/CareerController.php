<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;

class CareerController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $contentful = new Services\ContentfulCareers($request);

    $route = $request->getAttribute('route');
    $page = $route->getArgument('page');

    $this->getBreadcrumbs($this->data['navigation']['main'], "/academics/careers", $breadcrumbs);
    $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

    $this->data['queryParams'] = $request->getQueryParams();
    $this->data['currentPage'] = $request->getUri()->getPath();

    // Contentful needs to be called last, otherwise data wont be passed to it.

    $this->data['career'] = $contentful->getData($page, $this->data)['contentful'];
    $this->data['contentful'] = array('title' => $this->data['career']['title']." Career");
    $this->data['breadcrumbs'][] = array("title" => $this->data['contentful']['title']);

    return $this->data;

  }
}
