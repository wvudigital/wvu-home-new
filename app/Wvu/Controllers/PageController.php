<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;

class PageController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $contentful = new Services\Contentful($request);

    $route = $request->getAttribute('route');
    $page = $route->getArgument('page');

    $this->getBreadcrumbs($this->data['navigation']['main'], "/".$page, $breadcrumbs);
    $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

    $this->data['queryParams'] = $request->getQueryParams();
    $this->data['currentPage'] = $request->getUri()->getPath();

    // Contentful needs to be called last, otherwise data wont be passed to it.
    $this->data['contentful'] = $contentful->getData($page, $this->data)['contentful'];

    return $this->data;

  }
}
