<?php

namespace Wvu\Controllers;

use Wvu\Services;
use Wvu\Services\ApplicationVersion;
use Wvu\Services\Emergency;
use Wvu\Models;

class Controller {

  /**
  * Initialize the app var
  */
  public function __construct() {

    $app_version = new ApplicationVersion;
    $emergency = new Emergency($_ENV['EMERGENCY_JSON_URL'], 'emergency', 60);
    $navigation = new Models\Navigation;

    $this->data = [];
    $this->data['environment'] = $_ENV['ENVIRONMENT'];

    // TODO: Fix .revision error
    // Dot Revision file isn't being written for the time being use
    // Time stamp until can figure out the issue.
    // $this->data['app_version'] = $app_version->get();
    $date = new \DateTime();
    $this->data['app_version'] = $date->getTimestamp();

    $this->data['navigation'] = $navigation->data;
    $this->data['emergency'] = $emergency->getCache();
    $this->data['emergencyJson'] = json_encode($emergency->getCache());

  }

  protected function getBreadcrumbs($tree, $needle, &$result = array()) {
    $result = array();

    if (is_array($tree)) {
      foreach ($tree as $node) {

        if ($node['path'] == $needle) {
          $result[] = $node;
          return true;
        } else if (!empty($node['subnav'])) {
          if ($this->getBreadcrumbs($node['subnav'], $needle, $result)){
          $result[] = $node;
          return true;
          }
        }
      }
    } else {
      if ($tree == $needle) {
        $result[] = $tree;
        return true;
      }
    }
    return false;
  }

}
