<?php

namespace Wvu\Controllers;

use Wvu\Controllers;
use Wvu\Services;

class FAQController extends Controller {

  function __construct() {

    parent::__construct();

  }

  public function view($request) {

    $contentful = new Services\ContentfulFAQs($request);

    $route = $request->getAttribute('route');
    $page = $route->getArgument('page');

    $this->getBreadcrumbs($this->data['navigation']['main'], "/faqs", $breadcrumbs);
    $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

    $this->data['queryParams'] = $request->getQueryParams();
    $this->data['currentPage'] = $request->getUri()->getPath();

    // Contentful needs to be called last, otherwise data wont be passed to it.

    $this->data['faq'] = $contentful->getData($page, $this->data)['contentful'];
    $this->data['contentful'] = array('title' => $this->data['faq']['title']);
    $this->data['breadcrumbs'][] = array("title" => $this->data['contentful']['title']);

    return $this->data;

  }
}
