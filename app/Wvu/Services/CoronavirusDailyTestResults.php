<?php

namespace Wvu\Services;

class CoronavirusDailyTestResults {

  /**
  * Initialize the app var
  */
  public function __construct($request) {
    $this->request = $request;
  }

  public function getContent($slug = '') {

    $curl = $this->requestContent($slug);
    $this->data = json_decode($curl->response, true);

    return $this->data;

  }

  private function requestContent($options) {

    $curl = new \Curl\Curl;
    $this->setCurlOpts($curl);
    $path = ($options['useExpandedDashboard']) ? $_ENV['TESTRESULTS_EXPANDED_JSON_URL'] : $_ENV['TESTRESULTS_JSON_URL'];
    $url = $path.'?campus='.$options['relatedDivisionalCampus'].'&days='.$options['numberOfDaysShown'];
    if (isset($options['reportDayOfWeekStart'])) {
      $url .= '&dayOfWeekStart='.$options['reportDayOfWeekStart'];
    }
    if (isset($options['weeklyIsolationAndQuarantineReports'])) {
      $url .= '&weeklyIsolationAndQuarantineReports=true';
    }
    if (isset($options['dailyIsolationAndQuarantineReports'])) {
      $url .= '&dailyIsolationAndQuarantineReports='.$options['dailyIsolationAndQuarantineReports'];
    }
    //print $url;
    //exit;
    $response = $curl->get($url);

    $curl->close();
    return $response;

  }

  private function setCurlOpts($curl) {

    // TODO: Fix SSL verification
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
    // only allow 2 seconds for connection timeout
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT , 2);
    // allow up to 10seconds to get a response from rails server
    $curl->setOpt(CURLOPT_TIMEOUT, 10);

  }

}
