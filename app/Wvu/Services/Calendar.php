<?php

namespace Wvu\Services;

class Calendar extends CurlCache {

  protected function formatData($cached_data) {

    // Load XML and convert it down to a StdObject using json_encode & json_decode
    $eventsData = json_decode($cached_data);

    if (is_array($eventsData->events)) {

      foreach ($eventsData->events as $eventObj) {

        $event = $eventObj->event;

        // Convert DateTime Strings
        // $start_datetime = \DateTime::createFromFormat(\DateTime::ISO8601, $event->{'local-start-date-time'}.'-0500');
        // $start_datetime = \DateTime::createFromFormat('n/d/Y g:i A', $event->{'local-start-date'}." ".$event->{'local-start-time'});

        // latest
        //$start_str = $event->{'local-start-date'}." ".$event->{'local-start-time'};
        //$start_datetime = \DateTime::createFromFormat("n/j/Y g:i A", $start_str);

        // $end_datetime = \DateTime::createFromFormat(\DateTime::ISO8601, $event->{'local-end-date-time'}.'-0500');
        // $end_datetime = \DateTime::createFromFormat('n/d/Y g:i A', $event->{'local-end-date'}." ".$event->{'local-end-time'});

        // latest
        //$end_str = $event->{'local-end-date'}." ".$event->{'local-end-time'};
        //$end_datetime = \DateTime::createFromFormat("n/j/Y g:i A", $end_str);

        // handle Localist not being consistent with its time format
        $start_str = $event->event_instances[0]->event_instance->start;
        $start_datetime = (strpos($start_str,'T') !== false) ? \DateTime::createFromFormat(\DateTime::RFC3339, $start_str) : \DateTime::createFromFormat('Y-m-d H:i:s P', $start_str);
        $event->{'start_datetime_formatted_long'} = $start_datetime->format("D, F jS Y h:i:s A T");
        $event->{'start_datetime_formatted_short'} = $start_datetime->format("Y-m-d g:i A");
        $event->{'start_datetime_formatted_human'} = $start_datetime->format("M jS Y g:i A");

        if ($event->event_instances[0]->event_instance->end !== null) {
          $end_str = $event->event_instances[0]->event_instance->end;
          $end_datetime = (strpos($end_str,'T') !== false) ? \DateTime::createFromFormat(\DateTime::RFC3339, $end_str) : \DateTime::createFromFormat('Y-m-d H:i:s P', $end_str);
          $event->{'end_datetime_formatted_long'} = $end_datetime->format("D, F jS Y h:i:s A T");
          $event->{'end_datetime_formatted_short'} = $end_datetime->format("Y-m-d g:i A");
          $event->{'end_datetime_formatted_human'} = $end_datetime->format("M jS Y g:i A");
        }

      }

      $data = new \StdClass;
      $data = $eventsData->events;

      $this->data = $data;
    } else {
      // TODO: Debug Log events not being an array, issue with caching?
      $this->data = new \StdClass;
    }

  }
}
