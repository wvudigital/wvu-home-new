<?php

namespace Wvu\Services;

class CoronavirusPositiveCaseCounts {

  /**
  * Initialize the app var
  */
  public function __construct($request) {
    $this->request = $request;
  }

  public function getContent($slug = '') {

    $curl = $this->requestContent($slug);
    $this->data = json_decode($curl->response, true);

    return $this->data;

  }

  private function requestContent($slug) {

    $curl = new \Curl\Curl;
    $this->setCurlOpts($curl);
    $response = $curl->get($_ENV['CASECOUNTS_JSON_URL']);

    $curl->close();
    return $response;

  }

  private function setCurlOpts($curl) {

    // TODO: Fix SSL verification
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
    // only allow 2 seconds for connection timeout
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT , 2);
    // allow up to 10seconds to get a response from rails server
    $curl->setOpt(CURLOPT_TIMEOUT, 10);

  }

}
