<?php

namespace Wvu\Services;

class CurlCache {

  /**
  * Initialize the app var
  */
  public function __construct($url, $prefix, $ttl = null) {

    $cache_options = array();
    $cache_options['servers'][] = [$_ENV['MEMCACHE_SERVER'], $_ENV['MEMCACHE_PORT']];
    $cache_options['prefix_key'] = $_ENV['MEMCACHE_PREFIX'];
    $cache_options['serializer'] = 'php';
    $cache_options['libketama_compatible'] = true;
    $driver = new \Stash\Driver\Memcache($cache_options);

    $this->pool = new \Stash\Pool($driver);
    $this->data = new \StdClass;
    $this->url = $url;
    $this->item_prefix = $prefix;

    if ($ttl === null){
      $this->ttl = 600;
    } else {
      $this->ttl = $ttl;
    }
  }

  public function getCache() {

    $start_time = microtime(true);

    // Get item from cache
    $item = $this->pool->getItem($this->item_prefix);

    // Get data from item
    $cached_data = $item->get();

    // If item was not found in cache
    if ($item->isMiss()) {
      $cache_hit = false;
      // Get data from curl data
      $cached_data = $this->getContent()->response;

      // Set data to item
      $item->set($cached_data);
      $item->expiresAfter($this->ttl);

      // Save item to Cache
      $this->pool->save($item);
      $end_time = microtime(true);
    } else {
      $cache_hit = true;
      $end_time = microtime(true);
    }


    // format data from cache
    $this->formatData($cached_data);

    $data['content'] = $this->data;
    $data['cache']['hit'] = $cache_hit;
    $data['cache']['time'] = $end_time - $start_time. " seconds";

    return $data;
  }

  protected function formatData($data) {
    $this->$data = $data;
  }

  private function getContent() {
    print_r("<!-- running getContent ".$this->url." -->");
    $curl = new \Curl\Curl;
    $this->setCurlOpts($curl);
    $response = $curl->get($this->url);
    $curl->close();

    return $response;
  }

  private function setCurlOpts($curl) {
    // TODO: Fix SSL verification
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
    // only allow 2 seconds for connection timeout
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT , 2);
    // allow up to 10seconds to get a response from connection
    $curl->setOpt(CURLOPT_TIMEOUT, 10);
  }

}
