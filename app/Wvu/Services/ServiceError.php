<?php

namespace Wvu\Services;


class ServiceError extends \Exception {
  public $message;
  public $code;

  // Redefine the exception so message isn't optional
  public function __construct($message, $code = 0, Exception $previous = null) {

    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);
  }


}
