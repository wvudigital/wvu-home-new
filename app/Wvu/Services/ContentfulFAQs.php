<?php

namespace Wvu\Services;

class ContentfulFAQs {

  public function __construct($request) {
    $this->loader = new \Twig_Loader_Filesystem('../templates/');
    $this->twig = new \Twig_Environment($this->loader, array());
    $this->request = $request;
  }


  public function getData($slug, $appdata = []) {

    $data = [];

    $faqs = new FAQs($this->request);
    $response = $faqs->getContent(array('slug' => $slug));

    if (empty($response)) {
      throw new ServiceError('Empty Response.', 500);
      return $data;
    }

    // Decode Content
    $content = $response;
    /*print "foo";
    $content = json_decode($response,true);
    print_r($content);
    exit;*/

    if (array_key_exists('code', $content)){
      // if 404 Error
      if ($content['code'] == '404NotFound'){
        throw new ServiceError('404 Not Found.', 404);
        return $data;
      }
      // if 500 Error
      if ($content['code'] == 'Forbidden'){
        throw new ServiceError('Forbidden.', 500);
        return $data;
      }
    }

    // Merge contentful content with app data
    $appdata['contentful'] = $content;
    return $appdata;

  }

}
