<?php

namespace Wvu\Services;

class SiteIndex extends CurlCache {


  protected function formatData($curl) {

    // Manipulate data before saving it to cache
    $curl_obj = json_decode($curl,true);

    $this->data->non_alpha = [];
    $this->data->alpha = [];

    foreach($curl_obj as $key => $value){
      if (!ctype_alpha($key)){
        $this->data->non_alpha[] = $value;
      } else {
        $alpha_key = new \StdClass;
        $alpha_key->key = $key;
        $alpha_key->objects = $value;
        array_push($this->data->alpha, $alpha_key);
      }
    }
    // Fix nested array for non_alpha
    $this->data->non_alpha = array_map('reset', $this->data->non_alpha);
  }

}
