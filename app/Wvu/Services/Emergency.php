<?php

namespace Wvu\Services;

class Emergency extends CurlCache {


  protected function formatData($cached_data) {
    $events = [];
    $cached_events = json_decode($cached_data);

    if (is_array($cached_events->emergency->events)){
      foreach($cached_events->emergency->events as $event){
        array_push($events, $event->event);
      }

      $data['emergency']['events'] = $events;
      $data['emergency']['status'] = $cached_events->emergency->status;
      $data['emergency']['updated_at'] = $cached_events->emergency->updated_at;
      $data['emergency']['updated_at_formatted'] = $cached_events->emergency->updated_at_formatted;
      $data['emergency']['url'] = $_ENV['EMERGENCY_URL'];

    } else {
      $data['emergency']['events'] = [];
      $data['emergency']['status'] = false;
      $data['emergency']['updated_at'] = "";
      $data['emergency']['updated_at_formatted'] = "";
      $data['emergency']['url'] = "https://emergency.wvu.edu";
    }

    $this->data = $data;

  }

}
