<?php
namespace Wvu\Services;

class ApplicationVersion
{

    public static function get()
    {
        if ($_ENV['ENVIRONMENT'] == 'development'){
          $commitHash = trim(exec('git log --pretty="%H" -n1 HEAD'));
        } else {
          $revisionfile = fopen(__DIR__.'/../../../.revision','r');
          $commitHash = fgets($revisionfile);
        }
        return sprintf('%s', $commitHash);
    }
}
