<?php

namespace Wvu\Services;

class Wvutoday extends CurlCache {

  protected function formatData($data) {
    $data = json_decode(json_encode(simplexml_load_string($data,null,LIBXML_NOCDATA)),true);

    // Handle a S3 Access Denied Error
    if (is_array($data)){
      if (array_key_exists('Code',$data)){
        if ($data['Code'] == "AccessDenied"){
          $this->data = "";
        }
      } else {
        $data = array_slice($data['entry'], 0,3);
        foreach($data as $key => $entry){
          // remove extra spaces from link
          $link = str_replace(' ', '', trim($entry['link']));
          $date = \DateTime::createFromFormat("D, d M Y H:i:s O", $entry['date']);
          # DateTime::createFromFormat will return false if input isn't correct
          # Use todays date as default value if false
          if (!$date) {
            $date =  new \DateTime();
          }
          $date->setTimeZone(new \DateTimeZone(date_default_timezone_get()));
          $data[$key]['link'] =  $link;
          $data[$key]['datetime_formatted_long'] = $date->format("D, F jS Y h:i:s A T");
          $data[$key]['datetime_formatted_short'] = $date->format("Y-m-d g:i A");
          $data[$key]['datetime_formatted_human'] = $date->format("M jS Y g:i A");
          $data[$key]['date_formatted_long'] = $date->format("D, F jS Y");
          $data[$key]['date_formatted_short'] = $date->format("Y-m-d ");
          $data[$key]['date_formatted_human'] = $date->format("M jS Y");
        }
        $this->data = $data;
      }

    } else {
      $this->data = "";
    }
  }

}
