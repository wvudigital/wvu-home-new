<?php

namespace Wvu\Services;

class FAQs {

  /**
  * Initialize the app var
  */
  public function __construct($request) {
    $this->request = $request;
  }

  public function getContent($options = array()) {

    $curl = $this->requestContent($options);
    $this->data = json_decode($curl->response, true);

    return $this->data;

  }

  private function requestContent($options) {

    $curl = new \Curl\Curl;
    $this->setCurlOpts($curl);
    if (array_key_exists('q',$this->request->getQueryParams())){
      $q = urlencode(htmlspecialchars(strip_tags($this->request->getQueryParams()['q']), ENT_QUOTES));
    } else {
      $q = "";
    }

    if (isset($options['slug'])) {
      $response = $curl->get($_ENV['FAQS_ENTRY_URL'].urlencode($options['slug']));
    } else if ((count($options['groups']) > 0) && (!empty($q) && !is_null($q))) {
      $groupsQuery = "groups=".join(',',$options['groups']);
      $response = $curl->get($_ENV['FAQS_SEARCH_URL'].'&'.$groupsQuery.'&q='.$q);
    } else if (count($options['groups']) > 0) {
      $join = ($options['joinType'] === 'or') ? ',' : '*';
      $groupsQuery = "groups=".join($join,$options['groups']);
      $showAnswers = ($options['showAnswers']) ? 'showanswers' : 'default';
      $showAnswersQuery = "projection=".$showAnswers; // use a different projection to return body
      $response = $curl->get($_ENV['FAQS_JSON_URL'].'&'.$groupsQuery.'&'.$showAnswersQuery);
    } else if (!empty($q) && !is_null($q)) {
      $response = $curl->get($_ENV['FAQS_SEARCH_URL'].'&q='.$q);
    }

    $curl->close();
    return $response;

  }

  private function setCurlOpts($curl) {

    // TODO: Fix SSL verification
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
    // only allow 2 seconds for connection timeout
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT , 2);
    // allow up to 10seconds to get a response from rails server
    $curl->setOpt(CURLOPT_TIMEOUT, 10);

  }

}
