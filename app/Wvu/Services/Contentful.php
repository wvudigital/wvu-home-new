<?php

namespace Wvu\Services;

use \Stringy\Stringy as Stringy;

class Contentful {

  public function __construct($request) {
    // 'cache' => '../templates/cache',
    // 'auto_reload' => true
    $this->loader = new \Twig_Loader_Filesystem('../templates/');
    $this->twig = new \Twig_Environment($this->loader, [
      'cache' => '../templates/cache',
      'auto_reload' => true
    ]);
    $fetchFunction = new \Twig_Function('fetch', function ($url) {
      if ($url) {
          $curl = new \Curl\Curl;
          $curl->get($url);
          $curl->close();
          return $curl->response;
      }
    });
    $jsonDecodeFunction = new \Twig_Function('jsonDecode', function ($data) {
      if ($data) {
          $data = json_decode($data);
          return $data;
      }
    });
    $this->twig->addFunction($fetchFunction);
    $this->twig->addFunction($jsonDecodeFunction);
    $this->request = $request;
  }

  public function getData($slug, $appdata = []) {

    $data = [];

    $response = $this->getContent($slug)->response;
    if (empty($response)) {
      throw new ServiceError('Empty Response.', 500);
      return $data;
    }

    // Decode Content
    $content = json_decode($response,true);

    if (array_key_exists('code', $content)){
      // if 404 Error
      if ($content['code'] == '404NotFound'){
        throw new ServiceError('404 Not Found.', 404);
      }
      // if 500 Error
      if ($content['code'] == 'Forbidden'){
        throw new ServiceError('Forbidden.', 500);
      }
    }
    
    //fix for the case of api gateway returning just an ISE 'message' in the json body
    if (array_key_exists('message', $content)) {
      if ($content['message'] == 'Internal server error'){
        throw new ServiceError('Internal Server Error.', 500);
      }
    }

    // Merge contentful content with app data
    if (isset($content['blocks'])) {
      $params = $this->getParams();
      $content['blocks'] = $this->preProcess($content['blocks'],$params,false);
    }
    $appdata['contentful'] = $content;
    $data = $this->formatContent($appdata);
    return $data;

  }

  private function preProcess($items, $params, $reverse) {

    $itemsCopy = array();

    foreach ($items as $key => $value) {

      // need to test and splice in valid items in a personalization grouping
      if ((isset($value['contentfulContentType'])) && ($value['contentfulContentType'] === 'blocksPersonalizationGrouping') && (isset($value['personalizedBlocks']))) {

        $personalizedItems = [];
        $reverse = (isset($value['reversePersonalizationRules']) && ($value['reversePersonalizationRules']));
        foreach ($value['personalizedBlocks'] as $personalizedBlock) {
          if ($personalizedBlock['personalizationRulesMatchAnd'] && $this->testRules($personalizedBlock['personalizationRulesMatchAnd'],$params,$reverse,'and')) {
            $personalizedItems[] = $personalizedBlock['linkedBlock'];
          } else if ($personalizedBlock['personalizationRulesMatchOr'] && $this->testRules($personalizedBlock['personalizationRulesMatchOr'],$params,$reverse,'or')) {
            $personalizedItems[] = $personalizedBlock['linkedBlock'];
          }
        }

        if (count($personalizedItems) === 0) {
          foreach ($value['personalizedBlocks'] as $personalizedBlock) {
            if ($personalizedBlock['isDefault']) {
              $personalizedItems[] = $personalizedBlock['linkedBlock'];
            }
          }
        }

        $additionToArrayOffset = $key;
        $additionToArrayItems = $personalizedItems;
        if (count($personalizedItems) !== 0) {
          array_splice($items,$additionToArrayOffset,1,$additionToArrayItems);
        } else {
          array_splice($items,$additionToArrayOffset,1);
        }

      }

      // need to test an item with personalization options. skip itemsCopy if it doesn't match
      // then let item be evaluated for other things


    }

    foreach ($items as $key => $value) {
      if (is_array($value) && (!isset($value['personalizedBlocks']))) {
        $itemsCopy[$key] = $this->preProcess($value, $params, false);
      } elseif (is_string($value)) {
        $itemsCopy[$key] = $value;
      }
    }

    return $itemsCopy;

  }

  private function testRules($rules,$params,$reverse,$type) {

    $testResult = ($type == 'and');

    foreach ($rules as $rule) {
      $ruleResult = false;
      $rule['value'] = strtolower($rule['value']);
      $rule['key'] = strtolower($rule['key']);
      if (isset($params[$rule['key']])) {
        $values = explode('||',$rule['value']);
        foreach ($values as $value) {
          if (($value[0] === '!') && (substr($value, 1) !== $params[$rule['key']])) {
            $ruleResult = true;
          } else if ($value === $params[$rule['key']]) {
            $ruleResult = true;
          }
        }
      }

      if ($type == 'and') {
        $testResult = $testResult && $ruleResult;
      } else if ($type == 'or') {
        $testResult = $testResult || $ruleResult;
      }
    }

    if (($reverse) && ($testResult)) {
      $testResult = false;
    } else if (($reverse) && (!$testResult)) {
      $testResult = true;
    }

    return $testResult;

  }

  private function getParams() {

    $params = array();

    $headers = $this->request->getHeaders();
    foreach($headers as $k => $v) {
      if (strpos($k,'HTTP_CLOUDFRONT_') === 0) {
        $key = strtolower(str_replace('HTTP_CLOUDFRONT_','',$k));
        $params[$key] = strtolower($v[0]);
      }
    }

    $queryParams = $this->request->getQueryParams();
    foreach($queryParams as $k => $v) {
      $key = strtolower($k);
      $params[$key] = strtolower($v);
    }

    return $params;

  }

  private function getContent($slug) {

    $curl = new \Curl\Curl;
    $this->setCurlOpts($curl);
    $curl->get($_ENV['AWS_LAMBDA_URL'].urlencode($slug));
    $curl->close();

    return $curl;

  }

  private function formatContent($content) {

    $options = [];

    // swap out the theme either based on query string or the site itself
    if (array_key_exists('t',$this->request->getQueryParams()) && (($_ENV['ENVIRONMENT'] === 'development') || ($_ENV['ENVIRONMENT'] == 'staging'))) {
      $options['themeFolder'] = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);
    } else if (isset($content['contentful']['site']) && isset($content['contentful']['site']['theme']) && isset($content['contentful']['site']['theme']['themeFolder'])) {
      $options['themeFolder'] = $content['contentful']['site']['theme']['themeFolder'];
    }

    // get the current date for comparing against blocks
    $date = new \DateTime("NOW", new \DateTimeZone('America/New_York'));
    $options['timeNow'] = $date->format('U');

    if (array_key_exists('ts',$this->request->getQueryParams()) && (($_ENV['ENVIRONMENT'] === 'development') || ($_ENV['ENVIRONMENT'] == 'staging'))) {
      $ts = $this->request->getQueryParams()['ts'];
      if ((strlen($ts) === 10) && is_numeric($ts)) {
        $options['timeNow'] = $ts;
      }
    }

    // Match content blocks from Contentful to patterns
    $content['contentful']['renderedPreHeader'] = '';
    $content['contentful']['renderedPostHeader'] = '';
    $content['contentful']['rendered'] = '';
    $content['contentful']['renderedPreFooter'] = '';

    if (is_array($content['contentful']) && array_key_exists('blocks',$content['contentful'])) {

      if (isset($content['contentful']['site']) && isset($content['contentful']['site']['preHeaderBlocks'])) {
        $content['contentful']['renderedPreHeader'] = $this->renderBlocks($content['contentful']['site']['preHeaderBlocks'],$options);
      }
      if (isset($content['contentful']['site']) && isset($content['contentful']['site']['postHeaderBlocks'])) {
        $content['contentful']['renderedPostHeader'] = $this->renderBlocks($content['contentful']['site']['postHeaderBlocks'],$options);
      }
      $content['contentful']['rendered'] = $this->renderBlocks($content['contentful']['blocks'],$options);
      if (isset($content['contentful']['site']) && isset($content['contentful']['site']['preFooterBlocks'])) {
        $content['contentful']['renderedPreFooter'] = $this->renderBlocks($content['contentful']['site']['preFooterBlocks'],$options);
      }

    }

    return $content;

  }

  private function renderBlocks($blocks,$options) {

    $timeNow = $options['timeNow'];
    $themeFolder = isset($options['themeFolder']) ? $options['themeFolder'] : '';

    $rendered = '';

    foreach ($blocks as $block) {

      if
        (!isset($block['timeShow']) && !isset($block['timeHide']) ||
        (!isset($block['timeShow']) && (isset($block['timeHide']) && ($block['timeHide']['unix'] > $timeNow))) ||
        (!isset($block['timeHide']) && (isset($block['timeShow']) && ($timeNow >= $block['timeShow']['unix']))) ||
        ((isset($block['timeShow']) && ($timeNow >= $block['timeShow']['unix'])) && (isset($block['timeHide']) && ($block['timeHide']['unix'] > $timeNow))))
      {

        $type = Stringy::create($block['contentfulContentType']);

        switch($type) {
          case "blocksPrograms":

            $query = $this->request->getUri()->getQuery();
            if (isset($block["relatedDivisionalCampuses"]) || isset($block["relatedLevels"]) || isset($block["relatedHEPCLevels"]) || isset($block["relatedTypes"]) || isset($block["relatedColleges"]) || isset($block["relatedPathways"]) || isset($block["relatedKeywords"])) {
              $relatedKeys = ["relatedCampuses" => ["campusNameShort", "campus"], "relatedLevels" => ["title", "level"], "relatedHEPCLevels" => ["hepcLevelKey", "hepclevel"], "relatedTypes" => ["title", "type"], "relatedCollegesSchools" => ["abbreviation", "college"], "relatedPathways" => ["abbreviation", "pathway"], "relatedKeywords" => ["title", "tag"]];
              $queryOptions = [];
              foreach ($relatedKeys as $relatedKey => $relatedValue) {
                $join = array();
                if (isset($block[$relatedKey])) {
                  foreach ($block[$relatedKey] as $item) {
                    $join[] = str_replace(' ','',$item[$relatedValue[0]]);
                  }
                  $queryOptions[] = $relatedValue[1]."=".join($join,",");
                }
              }
              $query = join($queryOptions,"&");
            }
            $majors = new Majors($this->request);
            $options = array("query" => $query);
            $majors = $majors->getContent($options);

            if (is_null($majors)) {
              throw new ServiceError('Service Unavailable', 503);
              return $content;
            }

            if (array_key_exists('q',$this->request->getQueryParams())){
              $q = $this->request->getQueryParams()['q'];
            } else {
              $q = "";
            }
            if (!empty($q) && !is_null($q)) {
              $content['programs'] = $majors['entry'];
              $content['found'] = $majors['found'];
            } else {
              if (array_key_exists('code', $majors)){
                $content['programs'] = [];
                $content['found'] = 0;
              } else {
                $content['programs'] = $majors;
                $content['found'] = count($majors);
              }
            }

            break;

          case "blocksMajorMaps":
            $majorMaps = new MajorMaps($this->request);
            $majorMaps = $majorMaps->getContent();
            $content['programs'] = $majorMaps;
            $content['title'] = $block['title'];
            break;

          case "blocksCareers":
            $careers = new Careers($this->request);
            $careers = $careers->getContent();
            $content['careers'] = $careers;
            $content['title'] = $block['title'];
            break;

          /*
            case "blocksCoronavirusPositiveCaseCounts":
              $caseCounts = new CoronavirusPositiveCaseCounts($this->request);
              $caseCounts = $caseCounts->getContent();
              $content['caseCounts'] = $caseCounts;
              $content['title'] = $block['title'];
              break;
          */

          case "blocksCoronavirusDailyTestResults":
            //print "<pre>";
            //print_r($block);
            //print "</pre>";
            //exit;
            $testResults = new CoronavirusDailyTestResults($this->request);
            $options = array();
            $options['useExpandedDashboard'] = false;
            $options['numberOfDaysShown'] = 'all';
            if (isset($block['relatedDivisionalCampus'])) {
              $options['relatedDivisionalCampus'] = $block['relatedDivisionalCampus']['campusNameShort'];
            }
            if (isset($block['extraOptions']) && in_array('Use Expanded Dashboard', $block['extraOptions'])) {
              $options['useExpandedDashboard'] = true;
            }
            if (isset($block['numberOfDaysShown']) && in_array('Use Expanded Dashboard', $block['extraOptions'])) {
              $options['numberOfDaysShown'] = $block['numberOfDaysShown'][0];
            }
            if (in_array('Show Weekly Isolation and Quarantine Reports', $block['extraOptions'])) {
              $options['weeklyIsolationAndQuarantineReports'] = 'true';
            }
            if (in_array('Show Daily Isolation and Quarantine Reports', $block['extraOptions'])) {
              $options['dailyIsolationAndQuarantineReports'] = 'true';
            }
            if (in_array('Hide Daily Isolation and Quarantine Reports', $block['extraOptions'])) {
              $options['dailyIsolationAndQuarantineReports'] = 'false';
            }
            if (isset($block['reportDayOfWeekStart'])) {
              $options['reportDayOfWeekStart'] = $block['reportDayOfWeekStart'][0];
            }
            $testResults = $testResults->getContent($options);
            $content['testResults'] = $testResults;
            $content['title'] = $block['title'];
            $content['relatedDivisionalCampus'] = $block['relatedDivisionalCampus'];
            $content['extraOptions'] = $block['extraOptions'];
            break;

          case "blocksFrequentlyAskedQuestions":
            $faqs = new FAQs($this->request);

            $options = array();
            if (isset($block["relatedGroups"])) {
              $options['groups'] = array();
              foreach ($block["relatedGroups"] as $group) {
                $options['groups'][] = $group['abbreviation'];
              }
              if (isset($block["joinType"])) {
                $options['joinType'] = strtolower($block["joinType"][0]);
              } else {
                $options['joinType'] = 'or';
              }
            }

            $options['showAnswers'] = (isset($block["extraOptions"]) && in_array('Show Answers',$block["extraOptions"]));

            $faqs = $faqs->getContent($options);
            if (is_null($faqs)){
              throw new ServiceError('Service Unavailable', 503);
              return $content;
            }

            if (array_key_exists('q',$this->request->getQueryParams())){
              $q = htmlspecialchars(strip_tags($this->request->getQueryParams()['q']), ENT_QUOTES);
            } else {
              $q = "";
            }
            if (!empty($q) && !is_null($q)) {
              $content['faqs'] = $faqs['entry'];
              $content['found'] = $faqs['found'];
            } else {
              if (array_key_exists('code', $faqs)){
                $content['faqs'] = [];
                $content['found'] = 0;
              } else {
                $content['faqs'] = $faqs;
                $content['found'] = count($faqs);
              }
            }
            break;

          case "blocksScheduleIndex":
            $scheduledItems = new ScheduledItems($this->request);

            $options = array();
            if (isset($block["relatedScheduleCategories"])) {
              $options['categories'] = array();
              foreach ($block["relatedScheduleCategories"] as $category) {
                $options['categories'][] = $category['abbreviation'];
              }
              if (isset($block["categoryJoinType"])) {
                $options['categoryJoinType'] = strtolower($block["categoryJoinType"][0]);
              } else {
                $options['categoryJoinType'] = 'or';
              }
            }

            $scheduledItems = $scheduledItems->getContent($options);
            $content['scheduledItems'] = $scheduledItems;

            break;

        }

        $content['currentBlock'] = $block;
        $content['currentBlock']['timeNow'] = $timeNow;
        $content['currentBlock']['queryParams'] = $this->request->getQueryParams();

        // handle common attributes on blocks
        $content['currentBlock']['halveBottomPaddingClass'] = isset($block['extraOptions']) && in_array('Halve Bottom Padding',$block['extraOptions']) ? 'halve-bottom-padding' : '';
        $content['currentBlock']['halveTopPaddingClass'] = isset($block['extraOptions']) && in_array('Halve Top Padding',$block['extraOptions']) ? 'halve-top-padding' : '';
        $content['currentBlock']['removeBottomPaddingClass'] = isset($block['extraOptions']) && in_array('Remove Bottom Padding',$block['extraOptions']) ? 'remove-bottom-padding' : '';
        $content['currentBlock']['removeTopPaddingClass'] = isset($block['extraOptions']) && in_array('Remove Top Padding',$block['extraOptions']) ? 'remove-top-padding' : '';;
        $content['currentBlock']['removeLeftPaddingClass'] = isset($block['extraOptions']) && in_array('Remove Left Padding',$block['extraOptions']) ? 'remove-left-padding' : '';
        $content['currentBlock']['removeRightPaddingClass'] = isset($block['extraOptions']) && in_array('Remove Right Padding',$block['extraOptions']) ? 'remove-right-padding' : '';;
        $content['currentBlock']['inlineStyle'] = (isset($block['backgroundColor']) && ($block['backgroundColor'][0] === '#')) ? 'true' : 'false';

        $template_name = (($type == 'blocksBlank') && isset($block['template'])) ? 'blanks/'.str_replace('../','',$block['template']).'.html' : 'blocks/'.$type->dasherize().'.html';

        if (!empty($themeFolder)) {
          $themePath = __DIR__.'/../../../templates/'.$themeFolder.'/';
          if (is_dir($themePath)) {
            $this->twig->getLoader()->prependPath($themePath);
          }
        }

        $render = true;
        if (isset($block['extraOptions']) && in_array('Show Only When Check-in Cookie Exists',$block['extraOptions']) && (!isset($_COOKIE['wvu-hp-events-ci']) || ($_COOKIE['wvu-hp-events-ci'] != 'true'))) {
          $render = false;
        } else if (isset($block['extraOptions']) && in_array("Show Only When Check-in Cookie Doesn't Exist",$block['extraOptions']) && (isset($_COOKIE['wvu-hp-events-ci']) && ($_COOKIE['wvu-hp-events-ci'] == 'true'))) {
          $render = false;
        }

        if ($render) {

          if (isset($block['sidebar'])) {
            $content['currentBlock']['sidebar'] = $this->renderBlocks($block['sidebar'],$options);
          }

          try {
            $template = $this->twig->render($template_name,$content);
          } catch (\Twig_Error_Loader $e) {
            // Catch Error, otherwise do nothing and continue
            // TODO: Add logging
            $template = "";
            if ($_ENV['ENVIRONMENT'] == 'development'){
              $template = '<div class="homepage-error"><h2>Error Missing Template </h2><p>'.$e->getMessage().'</p></div>';
            }
            $rendered .= $template;
            continue;
          }

          $rendered .= $template;

        }

      }

    }

    return $rendered;

  }

  private function setCurlOpts($curl) {

    // TODO: Fix SSL verification
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
    // only allow 2 seconds for connection timeout
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT , 2);
    // allow up to 10seconds to get a response from rails server
    $curl->setOpt(CURLOPT_TIMEOUT, 10);

  }

}
