<?php

namespace Wvu\Services;

class Majors {

  /**
  * Initialize the app var
  */
  public function __construct($request) {
    $this->request = $request;
  }

  public function getContent($options) {

    $curl = $this->requestContent($options);
    $this->data = json_decode($curl->response, true);
    return $this->data;

  }

  private function requestContent($options) {

    $curl = new \Curl\Curl;
    $this->setCurlOpts($curl);

    if (array_key_exists('q',$this->request->getQueryParams())){
      $q = $this->request->getQueryParams()['q'];
    } else {
      $q = "";
    }
    if (isset($options['slug'])) {
      $response = $curl->get($_ENV['MAJORS_ENTRY_URL'].urlencode($options['slug'])."?gsi=bySlug");
    } else {
      if (!empty($q) && !is_null($q)) {
        $response = $curl->get($_ENV['MAJORS_SEARCH_URL'].'&'.$options['query']);
      } else {
        $response = $curl->get($_ENV['MAJORS_JSON_URL'].'&'.$options['query']);
      }
    }

    $curl->close();
    return $response;
  }

  private function setCurlOpts($curl) {

    // TODO: Fix SSL verification
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
    // only allow 2 seconds for connection timeout
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT , 2);
    // allow up to 10seconds to get a response from rails server
    $curl->setOpt(CURLOPT_TIMEOUT, 10);

  }

}
